#include<stdio.h>

#define SIZE 6
#define SWAP(A, B) {int T = A; A = B; B = T;}

void bubble_sort(int arr[]);
void print_array(int arr[]);


int main(void)
{
    //int arr[SIZE] = {44, 11, 55, 22, 66, 33};
    int arr[SIZE] = {11,22,33,44,55,66};
    print_array(arr);
    bubble_sort(arr);
    print_array(arr);

    return 0;
}

void bubble_sort(int arr[])
{
    int i,j;
    int pass=0, comp=0;
    int flag;
    // to count number of passes
    for(i = 0 ; i < SIZE - 1 ; i++)
    {
        flag = 0;
        pass++;
        // to count number of comparisions
        for(j = 0 ; j < SIZE - 1 -i ; j++)
        {
            comp++;
            // compare two consegative numbers
            if(arr[j] > arr[j+1])
            {
                SWAP(arr[j], arr[j + 1]);
                flag = 1;
            }
        }
        if(flag == 0)
            break;
    }
    printf("pass = %d, comp = %d\n", pass, comp);
}
/*
void bubble_sort(int arr[])
{
    int i,j;
    // to count number of passes
    for(i = 0 ; i < SIZE - 1 ; i++)
    {
        // to count number of comparisions
        for(j = 0 ; j < SIZE - 1 -i ; j++)
        {
            // compare two consegative numbers
            if(arr[j] > arr[j+1])
                SWAP(arr[j], arr[j + 1]);
        }
    }
}
*/
void print_array(int arr[])
{
    int i;
    printf("Array : ");
    for(i = 0 ; i < SIZE ; i++)
        printf("%4d", arr[i]);
    printf("\n");
}