#include<stdio.h>

#define SIZE 6

void insertion_sort(int arr[]);
void print_array(int arr[]);


int main(void)
{
    
    int arr[SIZE] = {55, 44, 22, 66, 11, 33};
    print_array(arr);
    insertion_sort(arr);
    print_array(arr);

    return 0;
}

void insertion_sort(int arr[])
{
    int i, j, temp;
    for(i = 1 ; i < SIZE ; i++)
    {
        temp = arr[i];      // backup
        for(j = i-1 ; j >= 0 && arr[j] > temp; j--)
        {
            arr[j+1] = arr[j];
        }
        arr[j+1] = temp;
    }
}

void print_array(int arr[])
{
    int i;
    printf("Array : ");
    for(i = 0 ; i < SIZE ; i++)
        printf("%4d", arr[i]);
    printf("\n");
}

            