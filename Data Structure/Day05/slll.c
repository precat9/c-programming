#include<stdio.h>
#include<stdlib.h>

// type declaration of node
typedef struct node
{
    int data;
    struct node *next;
}node_t;

node_t *head = NULL;

node_t *create_node(int data);
int is_list_empty(void);
void add_node_first(int data);
void add_node_last(int data);
void add_node_pos(int data, int pos);
void delete_node_first(void);
void display_list(void);

int main(void)
{
    add_node_first(44);
    add_node_first(33);
    add_node_first(22);
    add_node_first(11);
    add_node_last(55);
    add_node_last(66);
    //11->22->33->44->55->66
    //add_node_pos(77, 8);

    display_list();
    delete_node_first();
    display_list();

    return 0;
}

void delete_node_first(void)
{
    //1. if list is empty
    if(is_list_empty())
    {
    	//do nothing
        printf("List is empty\n");
        return;
    }
    //2. if list has only one node
    	//if(head->next == NULL)
    else if(head->next == NULL)
    {
        // free(head)
        free(head);
    }
    //3. if list has multiple nodes
    else
    {
    	// take backup of first node
        node_t *temp = head;    
        // create link between head and second node
        head = temp->next;
	    // free backuped node
        free(temp);
    }
}

void add_node_pos(int data, int pos)
{
    //1. create node with given data
    node_t *newnode = create_node(data);
    //2. if list is empty
    if(is_list_empty())
    {
    	// add newnode into head
        head = newnode;
    }
    else if(pos <= 1)
    {
        add_node_first(data);
        return;
    }
    //3. if list is not empty
    else
    {
	    //a. traverse list till pos-1
        int i = 1;
        node_t *trav = head;
        while(i < pos - 1 && trav->next != NULL)      //pos = 3
        {
            trav = trav->next;
            i++;
        }
    	//b. add pos node into newnode->next
        newnode->next = trav->next;	
    	//c. add newnode into tarv->next
        trav->next = newnode;
    }
}

void add_node_last(int data)
{
    //0. crate newnode with given data
    node_t *newnode = create_node(data);
    //1. if list is empty
    if(is_list_empty())
    {
	    //add newnode into head
        head = newnode;
    }
    //2. if list is not empty
    else
    {
	    // a. traverse till last node (trav->next != NULL)
        node_t *trav = head;
        while(trav->next != NULL)
            trav = trav->next;
	    // b. add newnode into trav->next
        trav->next = newnode;
    }
}

void display_list(void)
{
    //1. Create one trav variable and add head into it
    node_t *trav = head;
    printf("List : ");
    while(trav != NULL)
    {
        //2. print data of current node on console
        printf("%d ", trav->data);
        //3. go on next node
        trav = trav->next;
    }
    //repeat step 2 and 3 till trav != NULL
    printf("\n");
}

void add_node_first(int data)
{
    //1. create node dynamically
    node_t *newnode = create_node(data);
    //2. If list is empty
    if(is_list_empty())
    {
	    // add newnode into head only
        head = newnode;
    }
    //3. If list is not empty
    else
    {
	    //a. add head into newnode->next
        newnode->next = head;
	    //b. modify head to point newnode
        head = newnode;
    }
}

int is_list_empty(void)
{
    return head == NULL;
}

node_t *create_node(int data)
{
    // allocate memory dynamically to one structure object
    node_t *newnode = (node_t *)malloc(sizeof(node_t));
    // add data into created node
    newnode->data = data;
    newnode->next = NULL;
    // return newnode
    return newnode;
}