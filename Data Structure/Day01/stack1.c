#include<stdio.h>

#define SIZE 5

// declared the stack
int arr[SIZE];
int top;

void push(int data);
void pop(void);
int peek(void);
int is_empty(void);
int is_full(void);

int main(void)
{
    top = -1;
    int choice, data;
    do
    {
        printf("1. push\n2. pop\n3. peek\n");
        printf("Enter your choice :  ");
        scanf("%d", &choice);

        switch(choice)
        {
            case 1: // push
                printf("Enter data to be pushed : ");
                scanf("%d", &data);

                if(is_full())
                    printf("Stack is full\n");
                else   
                    push(data);

                break;

            case 2: // pop
                if(is_empty())
                    printf("Stack is empty\n");
                else
                    pop();
                break;

            case 3: // peek
                if(is_empty())
                    printf("Stack is empty\n");
                else   
                {
                    data = peek();
                    printf("Peeked Data  = %d\n", data);
                }
                break;
        }
    } while (choice != 0);
    

    return 0;
}

void push(int data)         // push data into stack
{
    // reposition of top (increment)
    top++;
    // add data at top index
    arr[top] = data;
}
void pop(void)          // delete the top  most data
{
    // reposition the top (decrement)
    top--;
}
int peek(void)          // collect the topmost data
{
    // return data at top index
    return arr[top];
}
int is_empty(void)      // check stack is empty or not
{
    if(top == -1)
        return 1;
    else
        return 0;
}
int is_full(void)       // check stack is full or not
{
    return top == SIZE - 1;
}