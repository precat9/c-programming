#include<stdio.h>
#include<stdlib.h>

// type declaration of node
typedef struct node
{
    int data;
    struct node *next;
}node_t;

node_t *head = NULL;

node_t *create_node(int data);
int is_list_empty(void);
void add_node_first(int data);
void delete_node_last(void);
void delete_node_pos(int pos);
void display_list(void);

int main(void)
{
    add_node_first(66);
    add_node_first(55);
    add_node_first(44);
    add_node_first(33);
    add_node_first(22);
    add_node_first(11);
        
    //11->22->33->44->55->66
    
    display_list();
    //delete_node_last();
    //11->22->33->44->55
    delete_node_pos(7);
    display_list();

    return 0;
}

void delete_node_pos(int pos)
{
    //1. if list is empty
    if(is_list_empty())
    {
	    // do nothing
        printf("List is empty\n");
        return;
    }
    else if(pos <= 1)
    {
        // call delete_node_first();
    }
    //2. if list is having only one node
    else if(head->next == NULL)
    {
	    // free(head)
        free(head);
        head = NULL;
    }
    //3. if list is having multiple nodes
    else
    {
	    // traverse till pos -1 node
        int i = 1;
        node_t *trav = head;
        while(i < pos - 1 && trav->next->next != NULL)
        {
            trav = trav->next;
            i++;
        }
	    // take backup of pos node
        node_t * temp = trav->next;
	    // create link between pos-1 and pos+1 node
        trav->next = temp->next;
	    // free pos node
        free(temp);
    }
}

void delete_node_last(void)
{
    //1. if list is empty
    if(is_list_empty())
    {
    	// do nothing
        printf("List is empty\n");
        return;
    }
    //2. if list is having only one node
    else if(head->next == NULL)
    {
    	// free(head)
        free(head);
        head = NULL;
    }
    //3. if list is having multiple nodes
    else
    {
        //a. traverse till second last node
        node_t *trav;
        for(trav = head ; trav->next->next != NULL ; trav = trav->next)
        {}
        //b. free(trav->next)
        free(trav->next);
	    //c. trav->next = NULL
        trav->next = NULL;
    }
}

void display_list(void)
{
    //1. Create one trav variable and add head into it
    node_t *trav = head;
    printf("List : ");
    while(trav != NULL)
    {
        //2. print data of current node on console
        printf("%d ", trav->data);
        //3. go on next node
        trav = trav->next;
    }
    //repeat step 2 and 3 till trav != NULL
    printf("\n");
}

void add_node_first(int data)
{
    //1. create node dynamically
    node_t *newnode = create_node(data);
    //2. If list is empty
    if(is_list_empty())
    {
	    // add newnode into head only
        head = newnode;
    }
    //3. If list is not empty
    else
    {
	    //a. add head into newnode->next
        newnode->next = head;
	    //b. modify head to point newnode
        head = newnode;
    }
}

int is_list_empty(void)
{
    return head == NULL;
}

node_t *create_node(int data)
{
    // allocate memory dynamically to one structure object
    node_t *newnode = (node_t *)malloc(sizeof(node_t));
    // add data into created node
    newnode->data = data;
    newnode->next = NULL;
    // return newnode
    return newnode;
}

            