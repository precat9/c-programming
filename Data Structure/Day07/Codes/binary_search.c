#include<stdio.h>

#define SIZE 9

int binary_search(int arr[], int key);

int main(void)
{
    int arr[SIZE]  = {11, 22, 33, 44, 55, 66, 77, 88, 99};
    int key, ret;

    printf("Enter key to be searched : ");
    scanf("%d", &key);

    ret = binary_search(arr, key);
    if(ret == -1)
        printf("Key is not found\n");
    else   
        printf("%d key is found at index %d\n", key, ret);

    return 0;
}

int binary_search(int arr[], int key)
{
    int left = 0, right = SIZE - 1, mid;

    while(left <= right)
    {
        //1. find mid of array
        mid=(left+right)/2;
        //2. compare key with middle element
        if(key == arr[mid])
        //3. if matching return mid
            return mid;
        //4. if key is less than middle element then update right of left sub array
        else if(key < arr[mid])
            right = mid - 1;
        //5. if key is greater than middle element then update left of right sub array
        else    //key > arr[mid]
            left = mid + 1;
    }
    //6. if key is not found return -1;
    return -1;
}
