#include<stdio.h>

#define SIZE 6
#define SWAP(A, B) {int T = A; A = B; B = T;}

void selection_sort(int arr[]);
void print_array(int arr[]);

int main(void)
{
    int arr[SIZE] = {44, 11, 55, 22, 66, 33};
    print_array(arr);
    selection_sort(arr);
    print_array(arr);

    return 0;
}

void selection_sort(int arr[])
{
    int i, j;
    for(i = 0 ; i < SIZE - 1; i++)
    {
        for(j = i + 1; j < SIZE; j++)
        {
            if(arr[i] > arr[j])
                SWAP(arr[i], arr[j]);
        }
    }

}
void print_array(int arr[])
{
    int i;
    printf("Array : ");
    for(i = 0 ; i < SIZE ; i++)
        printf("%4d", arr[i]);
    printf("\n");
}