#include<stdio.h>

#define SIZE 9

int linear_search(int arr[], int key);

int main(void)
{
    int arr[SIZE]  = {88, 33, 66, 99, 11, 77, 22, 55, 14};
    int key, ret;

    printf("Enter key to be searched : ");
    scanf("%d", &key);

    ret = linear_search(arr, key);
    if(ret == -1)
        printf("Key is not found\n");
    else   
        printf("%d key is found at index %d\n", key, ret);

    return 0;
}

int linear_search(int arr[], int key)
{
    int i;
    //1. Traverse array from 0 to SIZE-1
    for(i = 0 ; i < SIZE ; i++)
    {
        //2. compare ith element with key
        if(key == arr[i])
        //3. if key matching return i
            return i;
    }
    //4. if key not matching return -1;
    return -1;
}

            