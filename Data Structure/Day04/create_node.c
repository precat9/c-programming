#include<stdio.h>
#include<stdlib.h>

// type declaration of node
typedef struct node
{
    int data;
    struct node *next;
}node_t;

node_t *create_node(int data);

int main(void)
{
    node_t *n1 = create_node(10);
    
    printf("n1 data = %d\n", n1->data);

    free(n1);
    return 0;
}

node_t *create_node(int data)
{
    // allocate memory dynamically to one structure object
    node_t *newnode = (node_t *)malloc(sizeof(node_t));
    // add data into created node
    newnode->data = data;
    newnode->next = NULL;
    // return newnode
    return newnode;
}