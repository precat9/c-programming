#include<stdio.h>
#include<stdlib.h>
// type declaration of stack
typedef struct stack
{
    int *arr;
    int top;
    int size;
}stack_t;
//create a stack
stack_t st;

void init(void);
void push(int data);
void pop(void);
int peek(void);
int is_empty(void);
int is_full(void);
void free_stack(void);

int main(void)
{
    init();
    push(10);
    push(20);
    push(30);
    push(40);
    printf("Peeked element = %d\n", peek());        // 40
    pop();
    pop();
    printf("Peeked element = %d\n", peek());       // 20

    free_stack();
    return 0;
}

void init(void)
{
    // take size from user
    printf("Enter size of stack : ");
    scanf("%d", &st.size);
    //allcate memory dynamically to given size
    st.arr = (int *)malloc(sizeof(int) * st.size);
    // initialze top
    st.top = -1;
}

void free_stack(void)
{
    // free allocated memory
    free(st.arr);
    st.arr = NULL;
}

void push(int data)
{
    st.top++;
    st.arr[st.top] = data;
}
void pop(void)
{
    st.top--;
}
int peek(void)
{
    return st.arr[st.top];
}
int is_empty(void)
{
    return st.top == -1;
}
int is_full(void)
{
    return st.top == st.size - 1;
}