#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;
    static int count;

public:
    Complex(int real, int imag)
    {
        count++;
        this->real = real;
        this->imag = imag;
    }
    void print()
    {
        //cout << "Total count of objects = " << count << endl; //OK
        cout << "Real = " << this->real << endl;
        cout << "imag = " << this->imag << endl;
    }
    static void printCount()
    {
        cout << "Total count of objects = " << count << endl;
        //cout << "Real = " << this->real << endl;//Not OK
        //cout << "imag = " << this->imag << endl;//Not OK
    }
};
int Complex::count = 0; //initializing the static data member

int main()
{
    //Complex::printCount();

    Complex c1(10, 20);
    c1.print();

    Complex c2(100, 200);
    c2.print();

    //Complex::printCount();
    return 0;
}