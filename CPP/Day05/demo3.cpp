#include <iostream>
using namespace std;

class Arithmetic
{
private:
    int num1;
    int num2;

public:
    Arithmetic(int num1, int num2)
    {
        this->num1 = num1;
        this->num2 = num2;
    }
    void sum()
    {
        cout << "Addition of two nums = " << this->num1 + this->num2 << endl;
    }
    void sub()
    {
        if (this->num1 < this->num2)
            throw 1;
        cout << "Substraction of two nums = " << this->num1 - this->num2 << endl;
    }
    void mul()
    {
        if (this->num1 == 0 || this->num2 == 0)
            throw 1.2;
        cout << "Multiplication of two nums = " << this->num1 * this->num2 << endl;
    }
    void div()
    {
        if (this->num2 == 0)
            throw "Num2 cannot be 0";
        cout << "Division of two nums = " << this->num1 / this->num2 << endl;
    }
};
int main()
{
    Arithmetic a1(0, 10);
    try
    {
        a1.sum();
        //a1.sub();
        a1.mul();
        a1.div();
    }
    catch (int error)
    {
        cout << "Error code=" << error << endl;
        cout << "balance is less than the withdrawl amount" << endl;
    }
    catch (double error)
    {
        cout << "Error code=" << error << endl;
        cout << "0 cannot be given for multiplication" << endl;
    }
    catch (const char *err)
    {
        cout << err << endl;
        cout << "Don't provide 2nd value as 0" << endl;
    }
    catch (...)
    {
        cout << "Error. Technical Issues" << endl;
    }
    cout << "Sucessfully Executed" << endl;
    return 0;
}