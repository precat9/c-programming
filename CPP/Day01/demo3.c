#include <stdio.h>
//Enter time(hr,min,sec) from user and print it

struct Time
{
    int hr;
    int min;
    int sec;
};

void acceptTime(struct Time *t)
{
    printf("Enter hr,min and sec    :");
    scanf("%d%d%d", &t->hr, &t->min, &t->sec);
}

void printTime(struct Time t)
{
    printf("Time is = %d:%d:%d", t.hr, t.min, t.sec);
}

int main()
{
    struct Time t;
    acceptTime(&t);
    t.hr = 20; //OK
    printTime(t);
    return 0;
}
