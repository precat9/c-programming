#include <iostream>
//Enter time(hr,min,sec) from user and print it

struct Time
{
private:
    int hr;
    int min;
    int sec;

public:
    void acceptTime()
    {
        printf("Enter hr,min and sec    :");
        scanf("%d%d%d", &hr, &min, &sec);
    }

    void printTime()
    {
        printf("Time is = %d:%d:%d", hr, min, sec);
    }
};

int main()
{
    struct Time t;
    t.acceptTime();
    //t.hr = 20; // Not OK
    t.printTime();
    return 0;
}
