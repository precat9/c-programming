#include <iostream>
using namespace std;
void change_value(int ptr)
{
    ptr = 40;
}
void change_address(int *ptr)
{
    *ptr = 40;
}
void change_reference(int &ptr)
{
    ptr = 60;
}
int main()
{

    int num1 = 20;
    /*cout << "Value of num1 before change = " << num1 << endl;
    change_value(num1);
    cout << "Value of num1 before change = " << num1 << endl;
    change_address(&num1);
    cout << "Value of num1 after change = " << num1 << endl;

    //reference
    int &ref = num1;
    cout << "Value of num1 using reference= " << ref << endl;

    change_reference(num1);
    cout << "Value of num1 after change = " << num1 << endl;*/

    int *ptr = &num1;
    int &ref = num1;

    cout << "Address of num1 = " << &num1 << endl;
    cout << "Address of ptr = " << &ptr << endl;
    cout << "Address of ref = " << &ref << endl;

    return 0;
}