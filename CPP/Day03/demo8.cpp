#include <iostream>
using namespace std;

class Account
{
private:
    int accno;
    double balance;

public:
    Account(int accno, double balance)
    {
        this->accno = accno;
        this->balance = balance;
    }

    void setBalance(double balance)
    {
        this->balance = balance;
    }

    double getBalance()
    {
        return this->balance;
    }
    void printDetails()
    {
        cout << "Account no = " << this->accno << endl;
        cout << "Balance = " << this->balance << endl;
    }
};

int main()
{
    Account rohan(1, 500);
    rohan.printDetails();
    //rohan.setBalance(600, 1);
    rohan.setBalance(rohan.getBalance() + 600);
    cout << "Updated balance after Deposit =" << rohan.getBalance() << endl;
    //rohan.setBalance(300, 0);
    rohan.setBalance(rohan.getBalance() - 300);
    cout << "Updated balance after widthwral =" << rohan.getBalance() << endl;
    return 0;
}