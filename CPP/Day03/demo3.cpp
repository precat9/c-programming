#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    void accept() //(const Complex *this)
    {
        cout << "Enter the real and img value = ";
        cin >> real >> this->imag;
    }

    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << imag << endl;
    }
};

int main()
{
    Complex c1;
    c1.accept(); // accept(&c1)
    c1.print();  // print(&c1)

    Complex c2;
    c2.accept(); // accept(&c2)
    c2.print();  // print(&c2)
    return 0;
}