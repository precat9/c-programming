#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    void accept()
    {
        cout << "Enter the real and img value = ";
        cin >> real >> imag;
    }

    void print()
    {
        cout << "Real = " << real << endl;
        cout << "Imag = " << imag << endl;
    }
};

int main()
{
    Complex c1; //object of the clas Complex
    c1.accept();
    c1.print();
    return 0;
}