#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double num1 = 123.452;
    cout << setprecision(5) << num1;
    return 0;
}