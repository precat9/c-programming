#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex() // Parameterless Ctor
    {
        cout << "Inside Parameterless ctor" << endl;
        this->real = 10;
        this->imag = 20;
    }

    Complex(int real, int imag) // Parameterized Ctor
    {
        cout << "Inside Parameterized ctor" << endl;
        this->real = real;
        this->imag = imag;
    }

    void accept()
    {
        cout << "Enter the real and img value = ";
        cin >> this->real >> this->imag;
    }

    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    ~Complex() //Destructor
    {
        cout << "inside Dtor of :" << endl;
        print();
    }
};

int main()
{
    Complex c1; // parameterless ctor will be called
    c1.print();
    Complex c2(100, 200); //parameterized ctor will be called
    c2.print();
    return 0;
}