#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex() : real(10), imag(20) //Ctor members initializer list
    {
        cout << "Inside Parameterless ctor" << endl;
    }

    Complex(int real, int imag) : real(real), imag(imag) //Ctor members initializer list
    {
        cout << "Inside Parameterized ctor" << endl;
    }

    void accept()
    {
        cout << "Enter the real and img value = ";
        cin >> this->real >> this->imag;
    }

    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1; // parameterless ctor will be called
    c1.print();

    Complex c2(100, 300); //parameterized ctor will be called
    c2.print();
    return 0;
}