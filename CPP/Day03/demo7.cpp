#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex()
    {
        cout << "Inside Parameterless ctor" << endl;
        this->real = 10;
        this->imag = 20;
    }

    Complex(int real, int imag)
    {
        cout << "Inside Parameterized ctor" << endl;
        this->real = real;
        this->imag = imag;
    }

    void setImag(int imag) //Mutators or Setters
    {
        this->imag = imag;
    }

    void accept()
    {
        cout << "Enter the real and img value = ";
        cin >> this->real >> this->imag;
    }

    int getReal() //Inspector or Getter
    {
        return this->real;
    }
    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1;
    cout << "Value of only real = " << c1.getReal();
    c1.print();
    c1.setImag(56);
    c1.print();
    return 0;
}