#include <iostream>
using namespace std;
class Shape //abstarct class
{
public:
    virtual void calculateArea() = 0; // pure virtual function
    virtual void accept() = 0;
};

class Circle : public Shape
{
private:
    int radius;

public:
    Circle(int radius)
    {
        this->radius = radius;
    }
    void calculateArea()
    {
        cout << "Area of circle = " << 3.14 * this->radius * this->radius << endl;
    }
    void accept()
    {
        cout << "Enetr radius = ";
        cin >> this->radius;
    }
};

class Square : public Shape
{
private:
    int side;

public:
    Square(int side)
    {
        this->side = side;
    }
    void calculateArea()
    {
        cout << "Area of Square = " << this->side * this->side << endl;
    }
    void accept()
    {
        cout << "Enetr side = ";
        cin >> this->side;
    }
};

class Rectangle : public Shape
{
private:
    int length;
    int breadth;

public:
    Rectangle(int length, int breadth)
    {
        this->length = length;
        this->breadth = breadth;
    }
    void calculateArea()
    {
        cout << "Area of Rectangle = " << this->length * this->breadth << endl;
    }
    void accept()
    {
        cout << "Enetr length and breadt = ";
        cin >> this->length >> this->breadth;
    }
};

int main()
{
    Shape *s;
    Circle c(5);
    Rectangle r(10, 20);
    Square sq(5);
    int choice;
    do
    {
        cout << "0. Exit" << endl;
        cout << "1. Area of Rectangle" << endl;
        cout << "2. Area of Circle" << endl;
        cout << "3. Area of Square" << endl;
        cout << "Enter choice = ";
        cin >> choice;

        switch (choice)
        {
        case 1:
            s = &r;
            break;
        case 2:
            s = &c;
            break;
        case 3:
            s = &sq;
            break;
        }

        s->accept();
        s->calculateArea();
    } while (choice != 0);

    return 0;
}