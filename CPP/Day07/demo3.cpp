#include <iostream>
using namespace std;

class Base
{
public:
    void f1()
    {
        cout << "Base::f1()" << endl;
    }
    void f2()
    {
        cout << "Original Base::f2()" << endl;
    }
};

class Derived : public Base
{
public:
    void f2() // function overriding
    {
        cout << "Fake papers Derived:: f2()" << endl;
    }
    void f3()
    {
        cout << "Derived ::f3()" << endl;
    }
};

int main()
{
    Base b;
    b.f1(); //Base::f1()
    b.f2(); //Base::f2()

    Derived d;
    d.f1(); //Base::f1()
    d.f2(); //Derived::f2()
    d.f3(); //Derived::f3()
}