#include <iostream>
using namespace std;

class Base
{
public:
    void f1()
    {
        cout << "Base::f1()" << endl;
    }
    virtual void f2()
    {
        cout << "(Incomplete - Plot) Base::f2()" << endl;
    }
};

class Derived : public Base
{
public:
    void f2() //function overriding
    {
        cout << "(Complete-Home) Derived::f2()" << endl;
    }
    void f3()
    {
        cout << "Derived ::f3()" << endl;
    }
};

int main()
{
    Base *b;
    Derived d;
    b = &d;
    b->f1(); //Base::f1()
    b->f2(); //Complte::f2()

    d.f1(); //Base::f1()
    d.f2(); //Complete::f2()
    d.f3(); //Derived::f3()
}