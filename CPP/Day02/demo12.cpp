#include <iostream>
using namespace std;
//addition of 2 int numbers
//addition of 3 int numbers
//addition of 4 int numbers
//addition of 5 int numbers

//Default Argument Function
void sum(int num1, int num2, int num3 = 0, int num4 = 0, int num5 = 0)
{
    cout << "Addition of int nos = " << num1 + num2 + num3 + num4 + num5 << endl;
}

int main()
{
    sum(10, 20);
    sum(10, 20, 30);
    sum(10, 20, 30, 40);
    sum(10, 20, 30, 40, 50);
    return 0;
}