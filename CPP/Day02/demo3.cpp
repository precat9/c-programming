#include <iostream>

int num1 = 10;
int num2 = 20;

namespace na
{
    int num1 = 100;
    int num2 = 200;
    namespace na2 // nested namespace
    {
        int num1 = 1000;
        int num2 = 2000;
    }
}

int main()
{
    printf(" value of num1 = %d", ::num1);     //10
    printf(" \nvalue of num1 = %d", na::num1); //100

    printf(" \nvalue of num2 = %d", num2);     //20
    printf(" \nvalue of num2 = %d", na::num2); //200

    //i want to print num1 and num2 from nested namespace
    printf(" \nvalue of num1 = %d", na::na2::num1); //1000
    printf(" \nvalue of num2 = %d", na::na2::num2); //2000

    return 0;
}