#include <iostream>

int num1 = 10; //num1 is part of global namespace
int num2 = 20; //num2 is part of global namespace

namespace na //na is just a name -> take any name you like
{
    int num1 = 100;
    int num2 = 200;
}

int main()
{
    printf(" value of num1 = %d", ::num1);     //10
    printf(" \nvalue of num1 = %d", na::num1); //100
    printf(" \nvalue of num2 = %d", num2);     //20
    printf(" \nvalue of num2 = %d", na::num2); //200
    return 0;
}