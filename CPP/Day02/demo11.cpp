#include <iostream>
using namespace std;
//addition of 2 int numbers
//addition of 3 int numbers
//add 2 double nos

void sum(int num1, int num2)
{
    cout << "Addition of 2 int nos = " << num1 + num2 << endl;
}

void sum(int num1, int num2, int num3)
{
    cout << "Addition of 3 int nos = " << num1 + num2 + num3 << endl;
}

void sum(int num1, int num2, double num3)
{
    cout << "Addition of 3 int nos = " << num1 + num2 + num3 << endl;
}

void sum(double num1, double num2)
{
    cout << "Addition of 2 double nos = " << num1 + num2 << endl;
}

void sum(int num1, double num2)
{
    cout << "Addition of 1 int and 1 double nos = " << num1 + num2 << endl;
}

void sum(double num1, int num2)
{
    cout << "Addition of 1 double and 1 int nos = " << num1 + num2 << endl;
}

int main()
{
    int num1, num2;
    cout << "Enter num1 and num2";
    cin >> num1 >> num2;
    sum(num1, num2);
    sum(30, 10.30);
    sum(10.30, 20);
    sum(10.20, 40.50);
    sum(10, 20, 30);
    return 0;
}
