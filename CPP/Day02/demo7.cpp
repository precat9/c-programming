#include <iostream>
using namespace std;

int main()
{
    cout << "Hello World";
    cout << "\nHello World" << endl;
    cout << "\tHello World" << endl;
    cout << "Hel\blo World" << endl;
    cout << "Hello \rWorld" << endl;

    return 0;
}