#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex()
    {
        this->real = 0;
        this->imag = 0;
    }
    Complex(int real, int imag)
    {
        this->real = real;
        this->imag = imag;
    }

    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    Complex operator+(Complex &other) //(const Complex * this)//this->c1
    {
        Complex c;
        c.real = this->real + other.real;
        c.imag = this->imag + other.imag;
        return c;
    }
};

/*int sum(int num1, int num2)
{
    int result;
    result = num1 + num2;
    return result;
}*/

int main()
{
    Complex c1(10, 20);
    c1.print();
    Complex c2 = c1;
    c2.print();
    /*i want to create c3 object with c3.real value = addition of c1 and c2 real values
     and c3.imag value = addition of c1 and c2 imag values
        c3.real = c1.real+c2.real; c3(real =20)
        c3.imag = c1.imag+c2.imag; c3(imag =40)
   
    int result = sum(10, 20);
    cout << "Result =" << result;
     */
    Complex c3 = c1 + c2; // c1.operator+(c2)
    c3.print();
    //c3.add2objs(c1, c2);
    return 0;
}