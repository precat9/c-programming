#include <iostream>
using namespace std;

class Engine
{
private:
    int cc;
    double fuel;

public:
    void acceptEngine()
    {
        cout << "Enter cc and fuel capacity = " << endl;
        cin >> this->cc >> this->fuel;
    }
    void printEngine()
    {
        cout << "CC = " << this->cc << endl;
        cout << "Fuel Capacity = " << this->fuel << endl;
    }
};

class Car
{
private:
    Engine e;
    int price;

public:
    void acceptCar()
    {
        e.acceptEngine();
        cout << "Enter price = ";
        cin >> this->price;
    }
    void printCar()
    {
        e.printEngine();
        cout << "Price = " << this->price;
    }
};

int main()
{
    Car c1;
    c1.acceptCar();
    c1.printCar();
    return 0;
}