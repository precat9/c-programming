#include <iostream>
using namespace std;

class Engine
{
private:
    int cc;
    double fuel;

public:
    Engine(int cc, double fuel)
    {
        this->cc = cc;
        this->fuel = fuel;
    }
    void printEngine()
    {
        cout << "CC = " << this->cc << endl;
        cout << "Fuel Capacity = " << this->fuel << endl;
    }
};

class Car
{
private:
    Engine e;
    int price;

public:
    Car(int cc, double fuel, int price) : e(cc, fuel)
    {
        this->price = price;
    }
    void printCar()
    {
        e.printEngine();
        cout << "Price = " << this->price;
    }
};

int main()
{
    Car c1(1000, 1.2, 100000);
    c1.printCar();
    return 0;
}