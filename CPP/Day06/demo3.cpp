#include <iostream>
using namespace std;

template <typename T>
void sum(T num1, T num2)
{
    cout << "Addition = " << num1 + num2 << endl;
}

void sum(int num1, int num2, int num3 = 0, int num4 = 0)
{
    cout << "Addition of int for shop= " << num1 + num2 << endl;
}
void sum(double num1, double num2, double num3 = 0)
{
    cout << "Addition of double precised nos for income tax= " << num1 + num2 << endl;
}
// void sum(double num1, double num2)
// {
//     cout << "Addition = " << num1 + num2 << endl;
// }

int main()
{
    sum(10, 20);
    sum(10, 20, 30);
    sum(10.20, 20.30, 30.20);

    return 0;
}