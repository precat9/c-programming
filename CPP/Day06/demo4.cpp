#include <iostream>
using namespace std;
int num1;        //global variable -> Program scope
static int num2; // File Scope

namespace na
{
    int num1; // namespace scope
}

class A
{
    int num1; // class scope
};

void sum(int num1, int num2) //prototype scope
{
    int result; // Function scope
}

int main()
{
    {
        int num1; // Block scope
    }
}