#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex()
    {
        this->real = 0;
        this->imag = 0;
    }
    Complex(int real, int imag)
    {
        this->real = real;
        this->imag = imag;
    }

    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    friend Complex operator+(Complex &c1, Complex &c2);
};
Complex operator+(Complex &c1, Complex &c2)
{
    Complex c3;
    c3.real = c1.real + c2.real;
    c3.imag = c1.imag + c2.imag;
    return c3;
}

int main()
{
    Complex c1(10, 20);
    c1.print();
    Complex c2 = c1;
    c2.print();
    Complex c3 = c1 + c2; //operator+(c1,c2)
    //Complex c4 = c3 - c1; // Homework
    c3.print();
    return 0;
}