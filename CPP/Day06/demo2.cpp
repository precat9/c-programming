#include <iostream>
using namespace std;

template <class T, class X>
class Complex
{
private:
    T real;
    X imag;

public:
    Complex(T real, X imag)
    {
        this->real = real;
        this->imag = imag;
    }
    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex<double, int> c1(10.20, 20);
    c1.print();
    return 0;
}