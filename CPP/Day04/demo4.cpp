#include <iostream>
using namespace std;

int main()
{
    //int *ptr = (int *)malloc(1*sizeof(int))
    int *ptr = new int;
    *ptr = 20;
    cout << "Adress of dynamic memory allocated place = " << ptr << endl;
    cout << "Adress of ptr = " << &ptr << endl;
    delete ptr;
    ptr = NULL;
    return 0;
}
