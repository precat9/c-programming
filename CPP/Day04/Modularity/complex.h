#ifndef COMPLEX_H_
#define COMPLEX_H_
class Complex
{
private:
    int real;
    int imag;

public:
    Complex();
    Complex(int real, int imag);
    Complex(Complex &other); // Copy Ctor
    void setReal(int real);
    void setImag(int imag);
    int getReal();
    int getImag();
    void accept();
    void print();
};
#endif