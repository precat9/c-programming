#include <iostream>
#include "complex.h"
using namespace std;
int main()
{
    Complex c1(10, 20);
    c1.print();
    Complex c2;
    c2.print();
    c2.setReal(50);
    c2.print();
    return 0;
}