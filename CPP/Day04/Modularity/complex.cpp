#include <iostream>
#include "complex.h"
using namespace std;
Complex::Complex()
{
    this->real = 0;
    this->imag = 0;
}
Complex::Complex(int real, int imag)
{
    cout << "inside class ctor" << endl;
    this->real = real;
    this->imag = imag;
}
Complex::Complex(Complex &other) // Copy Ctor
{
    cout << "Inside copy ctor" << endl;
    this->real = other.real;
    this->imag = other.imag;
}
void Complex::setReal(int real)
{
    this->real = real;
}
void Complex::setImag(int imag)
{
    this->imag = imag;
}

int Complex::getReal()
{
    return this->real;
}
int Complex::getImag()
{
    return this->imag;
}
void Complex::accept()
{
    cout << "Enter num1 and num2 =" << endl;
    cin >> this->real >> this->imag;
}
void Complex::print()
{
    cout << "Real = " << this->real << endl;
    cout << "Imag = " << this->imag << endl;
}
