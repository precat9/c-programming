#include <iostream>
using namespace std;

int main()
{
    int num1 = 20;
    int *ptr = &num1;
    int &ref = num1;

    cout << "num1 value=" << num1 << endl;
    cout << "num1 value using ptr=" << *ptr << endl;
    cout << "num1 value using ref=" << ref << endl;

    return 0;
}