#include <iostream>
using namespace std;

class Complex
{
private:
    const int real;
    mutable int imag;

public:
    Complex(int real, int imag) : real(real)
    {
        //this->real = real;//const cannot be initialized here
        this->imag = imag;
    }

    void setImag(int imag)
    {
        this->imag = imag;
    }

    int getReal() const
    {
        return this->real;
    }
    int getImag() const
    {
        return this->imag;
    }

    void print() const
    {
        //this->imag = 30;
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    const int num1 = 10;
    Complex c1(10, 20);
    c1.print();
    const Complex c2(30, 40);
    c2.print();

    return 0;
}