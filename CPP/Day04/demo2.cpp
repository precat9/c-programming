#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex(int real, int imag)
    {
        cout << "inside class ctor" << endl;
        this->real = real;
        this->imag = imag;
    }
    Complex(Complex &other) // Copy Ctor
    {
        cout << "Inside copy ctor" << endl;
        this->real = other.real;
        this->imag = other.imag;
    }
    void print()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1(10, 20);
    c1.print();
    Complex c2 = c1;
    c2.print();
    return 0;
}