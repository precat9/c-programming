#include <iostream>
using namespace std;

class Complex
{
private:
    int *real;
    int imag;

public:
    Complex(int real, int imag)
    {
        this->real = new int;
        *this->real = real;
        this->imag = imag;
    }

    void setReal(int real)
    {
        *this->real = real;
    }
    void print()
    {
        cout << "Real = " << *this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
    ~Complex()
    {
        delete this->real;
        this->real = NULL;
    }
};
//Problem of shallow copy.
int main()
{
    Complex c1(10, 20);
    c1.print();
    Complex c2 = c1;
    c1.setReal(100);
    c1.print();
    c2.print();
    return 0;
}