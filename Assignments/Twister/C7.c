/*
#include <stdio.h>
int num;
int function(int n)
{
int num = 10;
return num;
}
int main(void)
{
printf("%d %d\n", num, function(20));
return 0;
}
0 10

*/

/*
#include <stdio.h>
int no1 = 17, no2 = 71;
void swapping(void)
{
int temp = no2;
no2 = no1;
no1 = temp;
}
int main(void)
{
int no1 = 17, no2 = 71;
printf("%d %d ", no1 , no2);
swapping();
printf("%d %d\n", no1, no2);
return 0;
}
17 71 17 71

*/

/*
#include <stdio.h>
int sunbeam()
{
int a=3;
return a * a;
}
int main(void)
{
int a=3;
printf("%d ", sunbeam(a));
return 0;
}
9
*/


#include <stdio.h>
int testDemo(int, int);
int main(void)
{
int you = 64, me = 32;
int we = testDemo(you, me);
printf("%d %d %d\n", me, you, we);
return 0;
}
int testDemo(int me, int you)
{
me = me + you;

return me - you;

you = you - me;

return me + you;
}


//32 64 64

