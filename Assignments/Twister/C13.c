









/*#include <stdio.h>
int main(void)
{
char str[] = "PrecatQuiz";
printf("%s %s %sn", &str[5], &5[str], str+5);
printf("%c %c %cn", *(str+6), str[6], 6[str]);
return 0;
}*/
/*
A. Runtime Error
B. Compiler Error
C. uiz uiz uiz u u u
*D. tQuiz tQuiz tQuiz nQ Q Qn*/




/*
#include <stdio.h>
int main(void)
{
int str[]={'P','R','E','C','A','T'};
printf("A%c ",str);
printf("A%s ",str);
printf("A%c ",str[0]);
return 0;
}
*/
/*
A. A A A
B. A AP AP
C. A [Garbage Value] AP AP
D. Compiler error
Answer: C*/



/*
#include <stdio.h>
int main(void)
{
char city[]="PUNE";
char *ptr=city;
while(*ptr != '\0')
{
printf("%c", *ptr);
ptr++;
};
return 0;
}
*/
/*
A. P
B. PUNE
C. Compiler error
D. None of the above
Answer: B*/

i = ++a[1];   2       
j = a[1]++;   2     a1=3
m = a[i++];   2




/*
#include <stdio.h>
int main(void)
{
int a[5] = {5, 1, 15, 20, 25};
int i, j, m;
i = ++a[1];          
j = a[1]++;
m = a[i++];

printf("%d, %d, %d", i, j, m);
return 0;
}
*/
/*
A. 3,2,15
B. 2,3,20
C. 2,1,15
D. 1,2,5
Answer: A
*/