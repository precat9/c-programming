












/*
#include<stdio.h>
int main(void)
{
int a = 10;
int *ptr = &a;
printf(" %d %d ", a,++*ptr);
printf(" %d %d ", a,*ptr++);
return 0;
}
//11 11 11 11
*/
/*
#include<stdio.h>
int print_size(int a[])
{
printf("%d, %d, ", sizeof(a) , sizeof( a[3] ) );
return 0;
}
int main(void)
{
int a[] = {1,2,3};
printf("%d, %d, ",sizeof(a) , sizeof( a[-1] ) );
print_size(a);
}
*/
/*
A. compile time error
B. 12 , 4 , 12 , 4
C. 12 , 4 , 4 , 4
D. 12 , 2 , 12 , 8
Answer: C
*/
/*
3. #include <stdio.h>
void callbyAddress1(int *x)
{
x=x+10;
}
void callbyAddress2(int *x)
{
*x=*x+10;
}
int main(void)
{
int a=10;
printf(" %d ",a);
callbyAddress1(&a);
printf(" %d ",a);
callbyAddress2(&a);
printf(" %d ",a);
callbyAddress1(&a);
printf(" %d ",a);
callbyAddress2(&a);
printf(" %d ",a);
return 0;
}
A. 10 10 20 20 30
B. 10 10 10 10 10
C. 10 20 30 40 50
D. compile time error
Answer: A
*/
/*
4. int main(void)
{
int arr1[];
int arr2[] = { 11, 22, 33 };
int arr3[3] = { };
int arr4[2] = { 10, 20, 30 };
int arr5[6];
int [] arr6;
return 0;
}
Which variable declarations are wrong?
A. arr1, arr3, arr4, arr6
B. arr1, arr3, arr6
C. arr1, arr4, arr6
D. None of these
Answer: C
*/

#include <stdio.h>
int main(void)
{
double arr[3];
char *p1, **p2;
printf("%u, ", sizeof(arr) + sizeof(arr[0]));
printf("%u\n", sizeof(*p1) + sizeof(*p2) + sizeof(p1) + sizeof(p2));
return 0;
}
/*
Note: consider 64-bit compiler.
A. 32, 25
B. 32, 28
C. 64, 25
D. None of these
Answer: A
*/