


/*
#include <stdio.h>
int ext = 30;
int main(void)
{
extern int ext;
printf("Ext = %d ", ext);
extfun();
return 0;
}
int ext = 10;
int extfun(void)
{
int ext = 20;
printf("%d\n",ext);
}

A. Ext = 10 20
B. Ext = 30 20
C. Compile time error
D. Run time error
Answer: C
*/

/*
2. #include <stdio.h>
static char char1 = 'A';
extern char char2 = 'B';
register char char3 = 'C';
void mystorage(void)
{
printf("%c %c %c\n", char1, char2, char3);
}
int main(void)
{
printf("%c %c %c\n", char1, char2, char3);
mystorage();
return 0;
}
A. A B C
A B C
B. Compile time error -
static variable cannot be declared globally
C. Compile time error -
extern variable cannot be declared globally
D. Compile time error -
register variable cannot be declared globally
Answer: D
*/
/*
3. #include <stdio.h>
int demo(char p1, char p2)
{
char p3;
p3 = ~p1 + ~p2;
return p3;
}
int main(void)
{
char p1 = 255, p2 = 256;
char p3 = demo(~p1++, ~p2--);
printf("%d %d %d\n", p1, p2, p3);
return 0;
}
A. -1 -1 0
B. -1 0 -1
C. 0 -1 -1
D. None of the above
Answer: C
*/


#include <stdio.h>
int i = 0;
int main(void)
{
    auto int i = 2;
    printf("%d ", i);//1
{
         i += 2;
        printf("%d ", i);//2
            {
                i = 1;
                printf("%d ", i);//3
            }
        printf("%d ", i);//3
}
    printf("%d", i);//1
    return 0;
}
/*
A. 0 1 2 2 0
B. 1 2 3 2 1
C. 1 2 3 3 1
D. 0 1 2 1 0
Answer: C
*/
/*
5. #include <stdio.h>
int my = 0;
int myset(int my)
{
printf("%d ", my++);
return my = my <= 2 ? 5 : 0;
}
int main(void)
{
int my = 5;
myset( my/2 );
printf("%d ", my);
myset( my=my/2 );
printf("%d ", my);
my = myset( my/2 );
printf("%d ", my);
return 0;
}
A. 3 5 3 2 2 5
B. 2 5 2 2 1 5
C. 2 3 2 2 2 5
D. 3 3 3 2 1 5
Answer: B
*/