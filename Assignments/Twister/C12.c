











/*
#include<stdio.h>
int main(void)
{
static char *s[] = {"eDAC", "eDMC", "eDBDA", "eDESD"};
char **ptr[] = {s, s+2, s+1, s+3};
char ***p=NULL;
p = &ptr[2];
++p;
printf("\n%s", **p+1);
--p;
//printf(" \n  %s", (**p+1)-1);//DMC
printf(" and %s", **p+1);
return 0;
}
//DESD and DMC

*/

//2. What will be the output of the following code?
//If following program having name cmdline is run from the command line as
//"cmdline eDAC eDBDA eDMC eDESD" //then what would be the output?
/*#include<stdio.h>
int main(int argc, char *argv[])
{
argv++;
printf("%c-",**++argv);       // increments location 
                              // %c prints char 
argv++;
printf("%s",++*argv);        //increment the value to which it is pointing
                            //.. print string
return 0;
}
*/
/*
A. e-DESD
B. e-DMC
C. e-DBDA
D. e-fDMC
E. e-fDESD
Answer: B
*/

//3. What will be the output of the following code?
/*#include<stdio.h>
void fun(int p[][3], int row, int col);
int main(void)
{
int a[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
fun(a,3,3);
return 0;
}
void fun(int p[][3], int row, int col)
{
int r,c;
for( c=0; c<row ; c++)
{
for(r=0; r<col; r++)
{
printf("%4d", *(*(p+r)+c) + p[c][r]);
}
printf("\n");
}
return;
}*/
/*
A. 2 6 10
   6 10 14
   10 14 18
B. 2 4 6
   8 10 12
   14 16 18
C.10 10 10
  10 10 10
  10 10 10
D. 2 8 14
   4 10 16
   6 12 18
Answer: A

*/

/*
4. What will be the output of the following code?
#include<stdio.h>
#include<string.h>
void fun1(char (*p)[4]);
void fun2(char (*p)[4]);
int main(void)
{
char arr[3][4] = { "ABC","DEF","GHI"};
char (*ptr)[4]=NULL;
ptr =arr;
fun1(ptr);
strcpy(arr[1],"MNO");
fun2(ptr);
return 0;
}
void fun1(char (*p)[4])
{
printf("%s ",(*(p+2)));
strcpy(p[2],"JKL");
}
void fun2(char (*p)[4])
{
strcpy(p[2],"JKL");
printf("%s ",(*(p+2)));
}
A. JKL JKL
B. GHI JKL
C. JKL GHI
D. GHI MNO
E. run time error
Answer: B
*/


//5. What will be the output of the following code?
#include<stdio.h>
void fun(int **p);
int main(void)
{
int a[][3] = {{11,2,3},{4,5,6},{7,20,40}};
int *ptr_a = a[2];
fun(&ptr_a);
return 0;
}
void fun(int **p)
{
**p = **p+1;
printf(" %d ", 1 - ++**p+1);
}
/*
A. runtime error
B. garbage value
C. -6
D. -7
E. -8
Answer: D

*/