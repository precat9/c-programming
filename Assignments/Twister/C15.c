
//1. What will be the output of the following code?
#include<stdio.h>
typedef struct 
{ 
    char c1[2]; 
}test_t; 
int main() 
{ 
    test_t t1; 
    t1.c1[0]=128; 
    t1.c1[1]=3; 
    printf("%d",*(short int *)t1.c1); 
    }
    /*
A. Runtime Error
B. 3
C. 896
D. 128
Answer: C
*/


//2. What will be the output of the following code?

/*include<stdio.h> 
typedef struct
{ 
    int id; 
    char bkname[20]; 
}BOOK;
int main() 
{ 
    BOOK b[] = {11,"The Secret",2,"The Alchemist"}; 
    char *p = ((char *)(b+1)) +4; 
    printf("%s",p); 
}
    
    A. Compile Time Error 
    B. The Secret 
    C. The Alchemist 
    D. Alchemist 
    E. Secret 
    Answer: C
    */

/*
//3. What will be the output of the following code?
#include<stdio.h>
struct Demo
{
int num1:4;
unsigned int num2:3;
int num3:2;
};
int main(void)
{
struct Demo d1;
d1.num1=5;
d1.num2=6;
d1.num3=-1;
printf("%d %d %d",d1.num1,d1.num2,d1.num3);
printf("\n");
d1.num1=10;
d1.num2=9;
d1.num3=3;
printf("%d %d %d",d1.num1,d1.num2,d1.num3);
return 0;
}
/*
A. 5 6 -1
-6 1 -1
B. 5 6 0
-6 1 0
C. 5 6 -1
-6 0 0
D. 5 6 -1
-6 -6 -1
Answer: A
*/


//4. Find the correct statement based on the following: 

/*#include<stdio.h> 
typedef struct 
{ 
    int id; 
    char bkname[20]; 
    typedef struct 
    { 
        int id; 
        char publisher[20]; 
        char contact[10]; 
        char address[100]; 
    }publish; 
}BOOK;

A. Declaration of data type will result in compile time error. 
B. It will help to declare a new data type BOOK. 
C. We cannot declare structure within structure 
D. None of these 
Answer: A
*/


/*
5. What will be the output of the following code?
#include<stdio.h> 
#pragma pack(4) 
typedef struct 
{ 
    unsigned b1 : 1; 
    unsigned b2 : 4; 
    unsigned b3 : 3; 
    char c; 
    }TEST;
int main() 
{ 
    printf("size = %u\n",sizeof(TEST)); 
}
A. 2 
B. 4 
C. 8 
D. 1 
Answer: B

*/










