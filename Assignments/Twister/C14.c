/*
#include<stdio.h>
#define print(Y,X) (Y/Y,X*Y)
int main( void )
{
printf("%d",print(5,9));
return 0;
}

A. 1
B. 81
C. 45
D. 0
Answer: C
*/


/*#include<stdio.h>
struct emp
{
int age;
struct emp *ptr;
};
int main(void)
{
struct emp var={20,NULL};
struct emp *ptr = &var;
ptr->ptr = ptr;
printf("%d %d",ptr->ptr->age,(*ptr).ptr->age);
}
A.20 20
B.20 NULL
C.0 0
D. Garbage Garbage
Answer: A
*/




/*
#include <stdio.h>
#pragma pack(1)
int main(void)
{
struct
{
int s[5];
union
{
char a; 
float b;
}u1;
} t;
printf("%d", sizeof(t) + sizeof(t.u1));//24+4
return 0;
}
A. 24
B. 28
C. 25
D. 20
Answer: B
*/



#include<stdio.h>
#include<stdlib.h>
int main( void )
{
int *a[3];
a = (int*) malloc(sizeof(int)*3);
free(a);
return 0;
}
/*
A. unable to allocate memory
B. compile time error as incompatible types
C. unable to free memory
D. no error
Answer : B
*/