/*#include <stdio.h>
int ext = 30;
int main(void)
{
extern int ext;
printf("Ext = %d ", ext);
extfun();
return 0;
}
int ext = 10;
int extfun(void)
{
int ext = 20;
printf("%d\n",ext);
}*/

/*#include <stdio.h>
static char char1 = 'A';
extern char char2 = 'B';
register char char3 = 'C';
void mystorage(void)
{
printf("%c %c %c\n", char1, char2, char3);
}
int main(void)
{
printf("%c %c %c\n", char1, char2, char3);
mystorage();
return 0;
}*/

/*#include <stdio.h>
int demo(char p1, char p2)
{
char p3;
p3 = ~p1 + ~p2;
return p3;
}
int main(void)
{
char p1 = 255, p2 = 256;
char p3 = demo(~p1++, ~p2--);
printf("%d %d %d\n", p1, p2, p3);
return 0;
}*/

/*#include <stdio.h>
int i = 0;
int main(void)
{
auto int i = 1;
printf("%d ", i);
{
int i = 2;
printf("%d ", i);
{
i += 1;
printf("%d ", i);
}
printf("%d", i);
}
printf("%d", i);
return 0;
}*/

#include <stdio.h>
int my = 0;
int myset(int my)
{
printf("%d ", my++);
return my = my <= 2 ? 5 : 0;
}
int main(void)
{
int my = 5;
myset( my/2 );
printf("%d ", my);
myset( my=my/2 );
printf("%d ", my);
my = myset( my/2 );
printf("%d ", my);
return 0;
}
