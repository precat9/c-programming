21. Four Iron metal rods of lengths 78 cm, 104 cm, 117 cm and 169 cm are to be cut into parts of equal length. Each part must be as long as possible. What is the maximum number of pieces that can be cut?
22. Which of the following years is not a leap year?
23. A truck covers a distance of 376 km at a certain speed in 8 hours. How much time would a car take at an average speed which is 18 kmph more than that of the speed of the truck to cover a distance which is 14 km more than that travelled by the truck ?
24. Today is Monday. After 61 days, it will be ...
25. Ten years ago, the sum of ages of a father and his son was 34 years. If the ratio of present ages of the father and son is 7:2, find the present age of the son.
26. There is meeting of 20 delegates that is to be held in a hotel. In how many ways these delegates can be seated along a round table, if three particular delegates always seat together?
27. 40 % of goods are sold at 10 % loss while the remaining is sold at 20 % profit. If there is total profit of Rs. 640, then the worth of goods sold is?
28. In a mixture of milk and water of volume 30 litres, the ratio of milk and water is 7 : 3. How much quantity of water is to be added to the mixture to make the ratio of milk and water 1 : 2?
29. An alloy contains14 parts of tin and 100 parts of copper. What is the percentage of tin in the alloy ?
30. Ramesh borrowed Rs. 3600 at a certain rate of interest C.I. and the sum grows to Rs. 4624 in 2 years. What is the rate of interest?
31. The first republic day of India was celebrated on January 26, 1950. What day of the week was it?
32. A shopkeeper sold some articles at Rs.35 per article and earned a profit of 40%. At what price each article should have been sold so that 60% profit was earned?
33. A train is moving at a speed of 132 km/hour. If the length of the train is 110 meters, how long will it take to cross a railway platform 165 meters long.
34. A vendor sells 50 percent of apples he had and throws away 20 percent of the remainder. Next day he sells 60 percent of the remainder and throws away the rest. What percent of his apples does the vendor throw?
35. The age of a man 10 years ago was thrice the age of his son. 10 years hence, the man's age will be twice the age of his son. The ratio of their present ages is = ?
36. Kiyan and Vicky start from a fixed point. Kiyan moves 4 km to the South, then turns left and moves 3 km. Vicky moves 3 km towards West, then turns right and moves 4 km further. How far apart are they now?
37. Statements
No fruits is a vegetables
All potatoes are vegetables
Some fruits are apples.
Conclusions:
I. No fruit is a potato.
II. At least some apples are fruits.
38. Sam walked 20 m towards the north. Then she turned right and walks 30 m. Then she turns right and walks 35 m. Then she turns left and walks 15 m. Finally she turns left and walks 15 m. In which direction and how many meters is she from the starting position?
39. If eraser is called box, box is called pencil, pencil is called sharpener and sharpener is called bag, what do we write with ?
40. P is the brother of N. O is the daughter of N. R is the sister of P and Q is the brother of O. Who is the uncle of Q?
41. Choose or find odd word:
Wheat , Barley, Rice , Chickpea, Mustard
42. Complete analogous pair: 
Auger : Carpenter :: Awl : ?
43. A + B means A is the father of B ;
A - B means A is the wife of B ;
A x B means A is the brother of B ;
A � B means A is the daughter of B.
If P x R + Q, which of the following is true ?
44. A, B, C, D, E, F, G and H are sitting around a circle, facing the center. A sits fourth to the right of H while second to the left of F. C is not the neighbor of F and B. D sits third to the right of C. H never sits next to G.

Who amongst the following sits between B and D?
45. Choose analogous pair-

Traffic : Road ::
46. In a certain code language, if Pen means Eraser, Eraser means Book, Book means Scale, Scale means Sharpener, Sharpener means Duster and Duster means Table, then what is the name of the object that is used to clean the black board in that language ?
47. Find the next in the sequence in the series: 
1, 3, 4, 8, 15, 27,_
48. Which village is to the south-west of village Q?
I. Village P is to the west of village N, which is to the east of village Q.
II. Village W is to the south of village N, which is to the west of village Q.
49. A, B, C, X, Y, Z are seated in a straight line facing North. C is third to the right of Z and B sits second to the right of C. X sits to the immediate right of A.

Which of the following represents the pairs of persons sitting exactly in the middle of the line?
50. If CERTAIN is coded as XVIGZRM, how can MUNDANE be coded ?
51. #include<stdio.h>
int main(void)
{
	int a = 0;
	for (a = 0; a<10; a++)
	{
		switch(a++)
		{
			case 0:a += 5;
			case 1:a += 2;
			case 5:a += 5;
			default: a += 4;
                        break;   
		}
		printf("%5d", a);
	}
	printf("\n");
	return 0;
}
52. PreProcessor replaces EXIT_SUCCESS symbol with --------
53. #include <stdio.h>
int f(int *a, int n)
{
	if(n <= 0) return 0;
	else if(*a % 2 == 0) return *a + f(a+1, n-1);
	else return *a - f(a+1, n-1);
}
int main()
{
	int a[] = {12, 7, 13, 4, 11, 6};
	printf("%d", f(a, 6));
	getchar();
	return 0;
}
54. Find the output of the following:
int main()
{
	float farr[] = {1.1,2.2,3.3,4.4,5.5};
	float *fp = farr+4;
	int x ;
	printf("%.2f",fp[-(x=sizeof(5.5)-6)]);
        return 0;
}
55. #include <stdio.h>
int fun(void)
{
	int a = 20, b = 48;
	a = a + b;
	return '48';
}
int main(void)
{
	printf("%d", sizeof(fun()));
	return 0;
}
56. Find the output of the following:
int main()
{
	typedef enum {O1=2 >= 5 != 4 ,O2= !(2 < 5 != 4)} OPR;
	OPR choice=0;
	do
	{
		switch(choice)
		{
			case O1:
					--choice;
					printf("%2d",choice);	break;
			case O2:
					printf("%2d",choice);	break;
		}
break;
	} while (++choice);	
	return 0;
}
57. #include <stdio.h>
int a = 23, b = 36;
void fun(int *p)
{
	*p = *p + *p;	
}

int main()
{
	int *p = &a;
	p = &b;
	fun(p);
	printf("a = %d, b = %d\n",a,b);
	return 0;
}
58. Find the output of the following:
int fun_test(int a,int b)
{
	a= !a;
	b = ~b;
	return a,b;
}
int main()
{
	int a=4,b=5;
	a,b = fun_test(!a,~b);
	printf("%d %d",a,b);
	return 0;

}
59. Find the output of the following:
#include<stdio.h>
typedef struct employee
{
    int id;
    char dept[10];
    struct  employee *emplist;
}EMP; 
int main()
{
    EMP e1={101,"ADMN",NULL};
    EMP e2={102,"MRKT",NULL};
    EMP e3={103,"TCT",NULL};
    EMP e4={104,"SAN",NULL};
    e1.emplist = &e2 ;
    e2.emplist = &e3;
    e3.emplist = &e4;
    printf("%d",e1.emplist->emplist->emplist->id);
    return 0;
}
60. Find the output of the following:
int main()
{
	int d;
	d = printf("Sunbeam") == printf("\rSunBeam") ;
	printf("%2d",d);
       return 0;
}
61. What is true about FILE *fp
62. What should be the output of the following code?
if file contents following data in rivers.txt
Neera Indrayani Venna Koyna
#include <stdio.h>
int main( void )
{
	FILE *fp=NULL;
	char c[1024];
	fp = fopen("rivers.txt", "r");
	fseek(fp, 0, SEEK_END);
	fseek(fp, -9L, SEEK_CUR);
	fgets(c, 5, fp);
	puts(c);
	return 0;
}
63. #include <stdio.h>
int main()
{
	int a = 10;
	int *p = &a;
	printf("%u\n", *&*&p);
	return 0;
}
Consider the address of 'a' variable is 3416378588
and address of 'p' pointer is 3416378592
64. Find the output of the following:
#include<stdio.h>
int main()
{
    FILE *fp = fopen("abc.txt","w+");
    int a,b,c;
    fprintf(fp,"%d %d %d",10,20,30);
    rewind(fp);
    printf("%d",fscanf(fp,"%d %d %d",&a,&b,&c));
    fclose(fp);
   return 0;
}
65. #include <stdio.h>
int main()
{
	int done = 0,  i = 0, x = 25;
	while(i < 10  && !done )
	{
		if((x /= 2) > 1)
		{
			i++;
			continue;
		}
		done++;
	}
	printf("x = %d\n",x);
	return 0;
}
66. Complete graph contains _____ no. of edges, if it contains "V" no. of vertices.
67. A data structure where elements can be added or removed at either end but not in the middle
68. Quick sort is also known as ��..
69. The no of external nodes in a full binary tree with n internal nodes is?
70. Which type of traversal of binary search tree outputs the value in sorted order?
71. What data structure would you mostly likely see in a non recursive implementation of a recursive algorithm?
72. The worst case occur in quick sort when
73. Where we should declare a function as a friend to a class?
74. ___________ is passed as a hidden argument to all nonstatic member function of class.
75. What is the output of following code?
#include<iostream>
using namespace std;
class C1 {
public:
   void print()  { cout <<"\n Inside C1"; }
}; 
class C2 : public C1 {
public:
   void print() { cout <<"\n Inside C2"; }
};  
class C3: public C2 { };  
int main(void)
{
  C3 c; 
  c.print();
  return 0;
}
76. ___________________ are used in object-oriented languages that set the accessibility of data members and  methods.
77. Class without name is called as _________.
78. ________member function can be called without creating any object.
79. An abstract base class is, that have _____________________.
80. Polymorphism means
81. If a base class is inherited in protected mode then which one of following is true?
82. What is the size of IP Address?
83. Disk Cache is present _____.
84. A _____ is a device that forwards packets between networks by processing the routing information included in the packet.
85. In Computer, FTP stands for
86. Which of the following components is not a part of the Modern CPU's?
87. Cache Memory is used
88. Arrangement of computer network nodes and connection between them is called
89. What is the full form of SCSI?
90. A set of rules that governs data communication
91. What is the term for a temporary storage area that compensates for differences in data rate and data flow between devices?
92. Semaphore is a/an _____________ to solve the critical section problem.
93. Which of the following is not a filesystem of Windows?
94. Memory Compaction involves
95. In which of the following IO technique/s CPU utilization is maximum?
96. In _____ ipc mechanism at a time one process can send message to another process.
97. Which of the following CPU Scheduling Algorithm/s is/are free from starvation?
98. _____ contains information to control disk operations.
99. The heads of the magnetic disk are attached to a ____________ that moves all the heads as a unit.
100. In Thrashing