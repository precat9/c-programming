NULL The origin of C is closely tied to the development of the Unix operating system, 
originally implemented in assembly language on a PDP-7 by Dennis Ritchie and Ken Thompson, 
incorporating several ideas from colleagues. Eventually, 
they decided to port the operating system to a PDP-11. 