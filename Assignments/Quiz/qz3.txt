10. 1. What will be the output of the following code? 
 #include <stdio.h>
     int main() {
	int count = 0, a = 0, b = 1;
	for (;;) {
		printf("#");
	if (count = a % 2 ? a++ : a-- ? a = 0 : ++b ? b = 2 : b++)
			break;
		printf("%d", ++count >> 1);
	}
	return 0;
}
Answers
1. #0#1#2#3#4#5#
2. #0#1#2#3#4#5#6###
*3. #
4. #0#1#2#3#4#5#6#7#

8. Find the correct statement about while loop from below:
I. It is necessary to include an increment/decrement statement. 
II. If no. of iterations are not fixed in advance it is compulsory to use while loop
*III. It helps to execute blocks of statements repeatedly till the modification statement does not support expression check to result false.
IV. It will execute a loop at least once.

9. 3. What will be the output of the following code? 
#include<stdio.h>
int main() {
	int  i = -3, j = 0;
	while (i <= 3, j < 10) {
		if (i >= 0)
			break;
		else {
			i++;
			j++;
		}
		printf("%d, %d\n", i, j);
		continue;
	}
	printf("\nSunbeam");
	return 0;
}
Answers
1. 2, 1     Sunbeam       -1, 2     Sunbeam        0, 3     Sunbeam
*2. -2, 1    -1, 2     0, 3     Sunbeam
3. Program will execute infinite no of times
4. Program will print sunbeam three times

7. 2.What will be the output of the following code?
#include<stdio.h>
int main() {
	int i = 0;
	for (printf("\nCDAC"); i < 2 && printf("\tSUNBEAMINFO");
			++i && printf("\tHINJEWADI")) 
	{
		printf("\t*n");
	}
	return 0;
}
Answers
1. CDAC    SUNBEAMINFO   *n   HINJEWADI       *n
2. Compile Time Error
3. CDAC    *n            *n
*4. CDAC    SUNBEAMINFO   *n   HINJEWADI       SUNBEAMINFO   *n   HINJEWADI 

6. 4) What will be the output of the following code?  
#include<stdio.h>
int main() {
	int sum = 0;
	unsigned int i = 20000;
	unsigned int j = 500;
	while (i-- / j++ > 1) {
		j += j;
		sum = sum + (i-- / j++);
		printf("\n%d, %d, %dn", i, j, sum);
	}
	return 0;
}
Answers
1. 19999, 1002, 19n 19998, 2006, 28n 19997, 4014, 32n 19996, 8030, 34n 19995, 16062, 35n
2. Compile Time Error
3. 19997, 1004, 17n 19996, 2008, 29n 19995, 4016, 35n 19994, 8032, 32n 19993, 16064, 31n
*4. 19998, 1003, 19n 19996, 2009, 28n 19994, 4021, 32n 19992, 8045, 34n 19990, 16093, 35n

5. #include <stdio.h>
int main()
{
    int loop=10;
    while(printf("Hello ") && loop--);//--loop-10 times
}
Answers
1. Hello Hello ... 10 times
*2. Hello Hello ... 11 times
3. Hello
4. Error: lvalue required as increment operand

4. Find the output of the following:
int main()
{
    int num=123,x;
    while(num , (num=num/10) , num--);
    {
        printf("%d",num);
    }
}
Answers
1. 123
2. 0
*3. -1
4. 12

3. What is the output of the following code? 
#include<stdio.h>
void main()
{
	int s=0;	
	while(s++<10)
	{
		if(s<4 && s<9)
			continue;
		printf(“\n%d\t”,s);
	}
}
Answers
1. 1 2 3 1 0
2. 4 5 6 7 8 9
*3. 4 5 6 7 8 9 10
4. 1 2 3 4 5 6 7 8 9

2. 8) What will be the output of following code?

#include <stdio.h>
    int main()
    {
        int i = 0, j = 0;
        while ( i < 2)
        { l1 : i++;
            while (j < 3)
            {
                printf("loop\n");
                goto l1;
            }
        }
    }
Answers
1. Loop Loop
2. Compilatiom Error
3. Loop Loop Loop Loop
*4. Infinite Loop

1. 9. What will be the output?
#include <stdio.h>
int main() {
	int x = 1, i = 10, ans;
	l1: i-- / --x;
	printf("%d", ans);

	for (;;) {

		{
			if (x > 5)
				continue;
			printf("\t%d", x++);
			goto l1;
		}
	}
	return 0;
}
Answers
1. Compile Time Error
2. 9       1       8       2       7       3       6       4       5       5
3. 9       1       9       2       7       4       6       4       5       4
*4. Run Time Error