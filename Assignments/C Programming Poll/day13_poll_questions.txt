DAY 12 Poll Questions
----------------------------

#include <stdio.h>
    int main()
    {
        register static int i = 10;
        i = 11;
        printf("%d\n", i);
    }

a) 10
b) Compile time error
c) Undefined behaviour
d) 11


Answer  : B


int main()
{
	register int a=10;
	{
		register int a = 15;
		printf("%d ", a);
	}
	printf("%d ", a);
	
	return 0;
}
A) 15 20
B) 15 10
C) 10 15
D) 15 15

Answer : B



What is the output the program.?

void my_recursive_function(int n)
{
    if(n == 0)
    return;
    printf("%d ",n);
    my_recursive_function(n-1);
}
int main()
{
    my_recursive_function(10);
    return 0;
}

a) 10
b) 1
c)10 9 8 7 6 5 4 3 2 1 0
d)10 9 8 7 6 5 4 3 2 1 

Answer: d





Which of the following statements is true?
a) Recursion is always better than iteration
b) Recursion uses more memory compared to iteration
c) Recursion uses less memory compared to iteration
d) Iteration is always better and simpler than recursion


Answer :B



#include <stdio.h>

int main(void)
{
extern int var=1000;
printf(" var = %d",++var);
return 0;
}
A. var = 1000
B. var = 0
C. var = 1001
D. compile time error
Answer: D







