DAY 8 Polling Questions
1) Which of the following is bit wise operator?

A. && operator
B. & operator
C. || operator
D. ! operator

Answer : B

2) int main() 
{ 
    
	unsigned char a = 5, b = 9; 
	printf("a&b = %d\n", a & b);
	return 0;
}

A.1
B.0
C.5
D.9

Answer: A 

5===> 0101
9===> 1001
=============
5&9==>0001




3) 
int main() 
{ 
    int x = 2, y = 5; 
    (x & y) ? printf("True ") : printf("False "); 
    (x && y) ? printf("True ") : printf("False "); 
    return 0; 
} 


A) False True
B) True False
C) True True
D) False False
Answer:A


x= 2 : 0010
y =5 : 0101
=============
x&y :  0000====> 0 ===> false

x && y
2 && 5
(exp1) && (exp2)
any non zero number means it is true (1)

1 && 1 ===> 1 ===> true








4) int main() 
{
	int a = 20;	
    	int b = 21;	
   	int c = 0;           
   	c = a ^ b;   
   	printf("C= %d\n", c );
   	return 0;
}

A) C=1
B) C=20
C) C=21
D) C=0

Answer :A 


//20 : 10100
//21 : 10101
==============
       00001 ===> 1 



&& logical AND
& Bitwise operation


