#include<stdio.h>

int main(void)
{
    int num=10;
    int *ptr=&num;
    int **pptr=&ptr;
    printf("Num = %d *ptr = %d",num,*ptr);
    printf("\n &Num = %d ptr = %d",&num,ptr);
    printf("\n size of ptr = %d",sizeof(ptr)); // 4bytes
     printf("\n size of pptr = %d",sizeof(pptr)); // 4bytes 
     //pointer size is fixed
     char ch='A';
     char *cptr=&ch;
    printf("\n ch = %c *cptr = %c",ch,*cptr);
    printf("\n Address : ch = %u  cptr = %u",&ch,cptr);
    printf("\n size of cptr = %d",sizeof(cptr));//4

    printf("\n size of *cptr = %d",sizeof(*cptr));//1
    
    return 0;
}


// *ptr
// ptr = address
//*ptr = value 