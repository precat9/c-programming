#include<stdio.h>
int main(void)
{
    int val=65;
    int *one;
    one=&val;    
    int **second;
    second = &one; // pointer to pointer is holding address of another pointer 
    printf("Val = %d Addres of val = %u",val,&val);//65
    printf("\n value inside first pointer = %d",*one);//65
    printf("\n address inside first pointer = %u",one);
    val=75;
    printf("\n value = %d *one = %d",val,*one);//75 75
    printf("\n *one = %d **second = %d",*one,**second);
    *one=95;
    printf("\n %d %d %d",val,*one,**second);//95 95 95
    return 0;
}


/*
int main(void)
{
    int num=50; // initialization (data store )
    int *p=&num; // pointer variable initialization (holds the address)
    // *p is pointing to data
    int **pp=&p; // pointer to pointer intialization 
   //pp hold the address of another pointer
   // **pp pointing to data 
   printf("Num= %d *p = %d **pp = %d",num,*p,**pp);  // 50 50 50
    num=60;
    printf("\n Num= %d *p = %d **pp = %d",num,*p,**pp);  // 60  60   60
    *p=70; // whereever pointer is pointing make the change in data as 70
     printf("\n Num= %d *p = %d **pp = %d",num,*p,**pp);  // 70  70   70
     **pp=85;
     printf("\n Num= %d *p = %d **pp = %d",num,*p,**pp);  // 85 85 85

    return 0;
}
*/

/*
int main(void)
{
    int num=20; // normal variable initialization
    int *ptr; //pointer declaration
    ptr=&num; // pointer definition

     //OR
    //int *ptr = &num; // pointer initialization

    int **pptr; // pointer to pointr
    pptr= &ptr; // pointer to pointer(pptr) is holding address of another pointer (ptr)

   
    printf("Num = %d &Num = %u",num,&num);
    //to print the address we use %d or %u 
    // address is in the form of unsigned int 
    printf("\n *ptr = %d ptr = %u",*ptr,ptr);
    printf("\n &ptr = %d",&ptr);

    printf("\n **pptr = %d pptr = %u",**pptr,pptr);
    printf("\n &pptr = %u",&pptr);

    return 0;
}

*/