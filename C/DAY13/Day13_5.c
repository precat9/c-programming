#include<stdio.h>


/*int main(void)
{
    int num=25;
    int *ptr=&num;
    printf("Num = %d *ptr = %d",num,*ptr);
    printf("\n &Num = %u ptr = %u",&num,ptr); // ptr = 2296 
    ptr = ptr+2; //2296 + 2 
    printf("\n &Num = %u ptr = %u",&num,ptr);
    ptr-=1;
    printf("\n &Num = %u ptr = %u",&num,ptr);
    return 0;
}*/

/*
int main(void)
{
    int num=25;
    int *ptr=&num;
    printf("Num = %d *ptr = %d",num,*ptr);
    printf("\n &Num = %u ptr = %u",&num,ptr);//76 76
    //*ptr++;
    //*2296++
    //*2300
    // Garbage value 
     //printf("\n ptr = %d *ptr = %d",ptr,*ptr);

    (*ptr)++;
    //(*2296)++;
    //25++
    //26
    printf("\n ptr = %d *ptr = %d",ptr,*ptr);
    return 0;
}
*/


/*
int main(void)
{
    int num=25;
    int *ptr=&num;
    printf("Num = %d *ptr = %d",num,*ptr);
    printf("\n &Num = %u ptr = %u",&num,ptr);
    *--ptr;
    // *--ptr
    //*--2296
    //*2292
    // not aware (garbage )
     printf("\n Num = %d *ptr = %d",num,*ptr); // 25  garbage 
    printf("\n ptr = %u ",ptr);

    return 0;
}
*/


/*

int main(void)
{
    int num=25;
    int *ptr=&num;
    printf("Num = %d *ptr = %d",num,*ptr);
    printf("\n &Num = %u ptr = %u",&num,ptr);
    --*ptr;
    // --*ptr
    //--*2296
    //--25
    //24 
     printf("\n Num = %d *ptr = %d",num,*ptr);


    return 0;
}

*/


/*
int main(void)
{
    int num=10;
    int *ptr=&num;
    printf("Num = %d *ptr = %d",num,*ptr);
    printf("\n &Num = %u ptr = %u",&num,ptr);
    
    //num++;
    //printf("\n Num = %d *ptr = %d",num,*ptr);

    //++num;
    //printf("\n Num = %d *ptr = %d",num,*ptr);

    //++*ptr;
    // ++*ptr
    // ++*2296
    // ++10
    // 11
    //printf("\n Num = %d *ptr = %d",num,*ptr); //11  11 

    *++ptr;
    //*++2296
    //* address + 1    //address+1 ===> next address 
                        //next address is based on scale factor of pointer
                        //ptr scale factor = 4bytes
                        // ++2296   ==> 2300 
                        // (POINTER ARITHMETIC )
    // *2300
    // value at 2300 (NO) ===> GARBAGE VALUE / ADDRESS 

    printf("\n Num = %d *ptr = %d",num,*ptr); // 20  garbage 
    printf("\n &Num = %u ptr = %u",&num,ptr);


    return 0;
}

*/