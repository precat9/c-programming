#include<stdio.h>

int main(void)
{
    int num=45;
    char ch='A';
    float f=5.5;

    void *ptr=NULL; //generic pointer (NULL POINTER )

    ptr=&num; // ptr is holding address of num
    printf("Num = %d &num = %u ptr = %u *ptr = %d",num,&num,ptr,*(int *)ptr);
    ptr= &ch; // ptr is holding address of char
    printf("\n ch  = %c &ch = %u ptr = %u *ptr = %c",ch,&ch,ptr,*(char *)ptr);
    ptr = &f; //ptr is holding address of float 
    printf("\n f  = %f &f = %u ptr = %u *ptr = %f",f,&f,ptr,*(float *)ptr);

    return 0;
}



/*

int main(void)
{
    //generic pointer
    void *vptr; //generic  pointer variable 
    int num=20;
    char ch='A';
    printf("Num = %d &num = %u Ch = %c &ch= %u",num,&num,ch,&ch);
    vptr=&num; // vptr is holding the address of num 
    printf("\n address inside vptr = %u",vptr);
    printf("\n contents at vptr = %d",*(int *)vptr);

    // num = 20   *(int *)vptr = 20 

    // void pointer need to be typecasted (converted)
    //in proper format so that it can fetch the number of bytes
    // based on scale factor


    vptr=&ch; // vptr is now holding address of ch
    printf("\n address inside vptr = %u",vptr);
    printf("\n contents at vptr = %c",*(char *)vptr);
    //while dereferencing the void pointer we should always
    // mention the scale factor by typecasting it

   

    return 0;
}


*/



/*
int main(void)
{
   float fval=4.5;
   float *fptr=&fval;
   printf("Fval = %f *fptr = %f",fval,*fptr);
    return 0;
}

*/