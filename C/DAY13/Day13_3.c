#include<stdio.h>

int main(void)
{
    int num=15; 
    //0000 0000 0000 0000 0000 0001 0000 1111
   
    char *c=&num; // pointer is of type char
    // scale factor 1byte

    printf("Num = %d * c = %d",num,*c);
    return 0;
}


/*
int main(void)
{
    int num=260;
    char *ptr=&num;
    printf("\n Num = %d *ptr = %d",num,*ptr);
    printf("\n Address Num = %u ptr = %u",&num,ptr);
    return 0;
}
*/



/*
int main(void)
{
    int num=260;
    int *ptr=&num;
    printf("\n Num = %d *ptr = %d",num,*ptr);
    printf("\n Address Num = %u ptr = %u",&num,ptr);
    return 0;
}

*/