#include<stdio.h>


int main(void)
{
    int num;
    char ch;
    printf("Enter Number ");
    scanf("%d",&num);
    printf("Enter Character ");
    scanf("%*c"); // this will skip the meaning of new line character 
    scanf("%c",&ch);
    printf("Num = %d Ch=%c",num,ch);
    printf("\n end of the program");
    return 0;
}



/*int main(void)
{
    int num;
    char ch;
    printf("Enter Character ");
    scanf("%c",&ch);
    printf("Enter Number ");
    scanf("%d",&num);
    
    printf("Num = %d Ch=%c",num,ch);
    printf("\n end of the program");
    return 0;
}
*/



/*int main(void)
{
    int num;
    char ch;
    printf("Enter Number ");
    scanf("%d",&num);
    printf("Enter Character ");
    scanf("%c",&ch);
    printf("Num = %d Ch=%c",num,ch);
    printf("\n end of the program");
    return 0;
}*/

//Expectation of my compiler 
// one number and one character 
// num 25 ===> &num 
// if we press 'enter' key from keyboard ====>  '\n' 
// '\n' it is a character value 
// as soon as we press enter from the keyboard 
// '\n' ====> &ch 
// &num ==> 25  &ch==> '\n' 
// compiler expectation is fullfilled ===> YES 
