#include<stdio.h>
 

 
 
 int main(void)
 {
     char ch='\n';
     printf("%d",sizeof(ch));
     return 0;
 }
 
 
 /*
int main(void)
{
    printf("ASCII Value of new line character = %d",'\n');          //10
    printf("\n ASCII Value of backslash character = %d",'\b');      //8
    printf("\n ASCII Value of tab character = %d",'\t');            //9
    printf("\n ASCII Value of carriage character = %d",'\r');       //13
    // '\n' - '\r'   // 10 - 13  // -3 
    return 0;
}*/





// Escape Sequence characters  OR  Back slash character constansts 
// 1. new line character \n  '\n' ===> 1byte 
//2. back space \b
//3. tab space \t
//4. carriage return \r




// Output
// Cprogramming - Day04\Program4
// Author : Akshita Sunbeam
// Date     24-11-2021

/*int main(void)
{
    printf("Good Morning");
    printf("\n Good Morning \r Great Morning");
            //  Great Morning 
            // \r will take a cursor on first character of same line
            // and starts overwriting (Home Button)
    printf("\n Good Morning \r Excellent");
    return 0;
}*/




/*int main(void)
{
    printf("Day04-cprogramming");
    printf("\nDay0\b4-cprogramming");
        //    Day4-cprogramming
    return 0;
}*/




/*int main(void)
{
    printf("CProgramming - Day04\\Program4");
    printf("\n Author : Akshita Sunbeam");
    printf("\n Date \t 24-11-2021");
    printf("\n**** \' hello \' \"Everyone\" *****  ");

    return 0;
}
*/







/*
int main(void)
{
    printf("Cprogramming");
    printf("\n day04"); //skip the orginility of letter n
    // escaping the meaning of letter n 
    // print new line
    // '\n' ==> single character
    return 0;
}

*/