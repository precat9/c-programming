#include<stdio.h>

//take input from the user at run time 
// scanf()

//Enter First Number
//45 ===> n1

//Enter Second Number
//35 ===> n2

//N1 = 45  N2 = 35 


/*
int main(void)
{
    int n1,n2; //variable declaration 
    printf("Enter First Number in integer format ");
    scanf("%d",&n1); //store the value at the address location of n1 
    printf("\n Enter Second Number :");
    scanf("%d",&n2); ///store the value at the address location of n2
    
    //scanf() is used to take input from user 
    //& means address operator

   // printf("Enter two numbers");
   // scanf("%d %d",&n1,&n2); //allowed // Not recommended 

    printf("First Number = %d Second Number  = %d",n1,n2);





    return 0;
}

*/








/*
int main(void)
{
    int n1=20; // variable initialization
    int n2=25;// variable initialization
    printf("N1 = %d N2 = %d",n1,n2);

    return 0;
}
*/
