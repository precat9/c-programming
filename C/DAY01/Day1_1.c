

//COMMENTS : Used for Documentation / Information to you 
//1. Single Line Comment   // 
//2. Multiple Line Comment  /*  ..... */ 

// Objective : Display Welcome All OM27 Batch Students on output screen

/*
Author : Akshita Chanchlani
Organization : Sunbeam
Date: 19th Nov 2021 
*/


//predefined features are included in some files 
// files Header files 

// File inclusion area 
#include<stdio.h> // stadndard input output .h (headerfile)
int main()
{
    printf("Welcome All OM27 Batch Students"); // end of statement 
    return 0;
}  // SCOPE OF PROGRAM 



// printf() is a function which is used to print the data on output screen 
// printf() function is already defined
// in the header files 

// Entry point Function / main function 


// After wrtiting the program
// We need to compile the code
// gcc Day1_1.c
// Then we need to execute the code
// ./a.exe (Executable File)



// ./a.exe   OR  .\a.exe 
