#include<stdio.h>

// Formatting the output by mentioning the width specifiers 
int main()
{
    float fval=5.6435;
    printf("Fval = %f",fval); // by default 6 digits displayed
    printf("\n Fval = %.2f",fval); // .2f it will display 2 digits 
    printf("\n Fval = %9.2f",fval); 
    //  _ _ _ _ _ 5 . 6 4 
    //9.2 indicates , please assign 9 spaces width and after decimal display 2 digits 

    return 0;
}

/*
int main(void)
{
    int num1=25;
    int num2=400;
    int num3=5000;
    printf("\n%5d %5d %5d",num1,num2,num3);
    printf("\n%5d %5d %5d",num2,num1,num3);
    printf("\n%5d %5d %5d",num3,num1,num2);

    printf("\n%-5d %-5d %-5d",num1,num2,num3);
    printf("\n%-5d %-5d %-5d",num2,num1,num3);
    printf("\n%-5d %-5d %-5d",num3,num1,num2);


    return 0;
}
*/

/*
int main(void)
{
    int num1=25;
    int num2=400;
    int num3=5000;
    printf("%d%d%d",num1,num2,num3);  
    printf("\n%5d %5d %5d",num1,num2,num3); // right allignment (Formatting the output)  
    // _ _ _ 2 5   _ _ 4 0 0   _ 5 0 0 0 
    // _ _ _ _ _   _ _ _ _ _   _ _ _ _ _  
    printf("\n%d %d %d",num1,num2,num3);   
    printf("\n%-5d %-5d %-5d",num1,num2,num3);  // left allignment (Formatting the output)  
     // 2 5 _ _ _   4 0 0 _ _   5 0 0 0 _
     // _ _ _ _ _   _ _ _ _ _   _ _ _ _ _ 
    return 0;
}

*/