#include<stdio.h>

int main(void)
{
    int num=0x32; // Hexa Number
    // if any number preceded with 0x then it is considered as hexa decimal number 
    // by the compiler   
    printf("Num = %o Num = %d Num = %x",num,num,num);
    //%o ==> octal number
    //%x ==> hexa numbers
    // %d ==> int numbers 
    return 0;
}





/*
int main(void)
{
    int num=064; // Octal Number
    // if any number preceded with 0 then it is considered as octal number 
    // by the compiler   
    printf("Num = %o Num = %d Num = %x",num,num,num);
    //%o ==> octal number
    //%x ==> hexa numbers
    // %d ==> int numbers 
    return 0;
}

*/



/*
int main(void)
{
    int num=64; // 64 is whole number integer 
    printf("Num = %d",num); // %d 
    return 0;
}

*/