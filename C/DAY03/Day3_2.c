#include<stdio.h>

/*
int main(void)
{
    double d=6.4;
    printf("D Value = %f",d);  // D Value = 6.4.....
    printf("\n size of double = %d",sizeof(double));  // 8
    printf("\n size of variable d = %d",sizeof(d)); // 8
    printf("\n size = %d",sizeof(85.68)); // 8 
     printf("\n size = %d",sizeof(85.68f)); // 4
    
    
    return 0;
}

*/




/*
int main(void)
{
    float n1=6.78;
    float n2=8.795;
    printf("N1 value = %f",n1); // value 
    printf("\n size of n1 variable = %d",sizeof(n1)); 
    printf("\n size of float datatype = %d",sizeof(float));

    printf("\n N2 value = %f",n2);

    printf("\n size of any specific decimal number = %d",sizeof(3.45));
    // direct value / hardcoded value of type decimal  in sizeof() 
    // by default it is considered as double datatype 
    // size = 8 bytes
    printf("\n size of any specific decimal number = %d",sizeof(3.45f));
    // direct value / hardcoded value of type decimal in sizeof() preceeded with 'f'
    // it will be considered as a float value datatype
    // size = 4 bytes 
    return 0;
}

*/