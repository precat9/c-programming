#include<stdio.h>

//const keyword 

int main(void)
{ 
    const float PI=3.14; //constant value 
    const float * const fptr=&PI; // declaration of float pointer which is cons
    // pointer contents are constant
    //as well as location at which pointer is pointing that is also constant
    printf("\n PI = %f &PI = %u",PI,&PI);
    printf("\n *fptr = %f fptr= %u",*fptr,fptr);

   float fval=6.5; // non constant
   //fptr=&fval;   //error: assignment of read-only variable 'fptr'
   //printf("\n *fptr = %f fptr= %u",*fptr,fptr);

    return 0;
}

/*
// float f1 = 5.4;
// f1++; //allowed 
// f1=7.5;  //allowed

// const float f2=2.3;
// f2++; // not allowed
//f2=6.7; // not allowed 

// const float *ptr;  //location was constant 
// ptr = &f1;  *ptr = 2.3;   not allowed
// ptr = &f2;  *ptr = 6.467;  not allowed 

// float *pt;
// pt=&f1;  *pt = 50.3;  //allowed
// pt = & f2;  *pt=67.8; // allowed 


*/



/*
int main(void)
{
    const float PI=3.14; //constant value 
    const float *fptr; // declaration of float pointer which is constant
    float fval=7.8;  //noraml float variable 

    fptr=&PI; // // fptr is holding address of PI (constant value )
     printf("\n PI = %f &PI = %u",PI,&PI);
     printf("\n *fptr = %f fptr= %u",*fptr,fptr);
     //PI++; // not allowed  error: increment of read-only variable 'PI'
    //*fptr=5.15; //error: assignment of read-only location '*fptr'
    fptr=&fval; // fptr is holding address of fval (non constant value)
    printf("\n FVAL = %f &FVAL = %u",fval,&fval);
     printf("\n *fptr = %f fptr= %u",*fptr,fptr);
    // *fptr=9.9; //error: assignment of read-only location '*fptr'


    fval=7.543; // ALLOWED 
    //PI=5.5; //NO 
    return 0;
}
*/

/*
int main(void)
{
    int num=25;
    int *ptr = &num;
    const float PI=3.14; //constant value 
    float *fptr = &PI; // fptr is holding address of PI 

    printf("Num = %d &num= %u",num,&num);
     printf("\n *ptr = %d ptr= %u",*ptr,ptr);
     printf("\n PI = %f &PI = %u",PI,&PI);
     printf("\n *fptr = %f fptr= %u",*fptr,fptr);

     *ptr = 45;
     printf("\n Num = %d *ptr= %d",num,*ptr);

     *fptr=20.45;
      printf("\n PI = %f *fptr  = %f",PI,*fptr);


     return 0;
}
*/


/*
int main(void)
{
    int num=20;
     printf(" Num = %d",num);
    num++; // allowed 
    printf("\n  Num = %d",num);
    const float PI=3.14; //constant variable 
    printf("\n PI  = %f",PI);
    //PI++; // not allowed  //error: increment of read-only variable 'PI
    //printf("\n PI  = %f",PI);
    return 0;
}
*/


/*
int main(void)
{
    int num=20;
     printf(" Num = %d",num);
    num++; // allowed 
    printf("\n  Num = %d",num);
    float PI=3.14;
    printf("\n PI  = %f",PI);
    PI++; // allowed 
    printf("\n PI  = %f",PI);
    return 0;
}
*/