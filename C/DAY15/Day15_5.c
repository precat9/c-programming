#include<stdio.h>


/*
int main(void)
{
    char str[]="sunbeam";
    //printf("str[0] = %c  *str = %c",str[0],*str);//s s
    //printf("\n str[1] = %c  *(str+1) = %c",str[1],*(str+1));//u u
   //printf("\n str[2] = %c  *(str+2) = %c",str[2],*(str+2));//n n
   // printf("\n str[3] = %c  *(str+3) = %c",str[3],*(str+3));//b b

    //printf("\n *str+1 = %c",*str+1);//
    //*str+1
    // *baseaddress + 1
    // s + 1
    // t 

    // printf("\n *str+4 = %c",*str+4); // s ==> t u v w 
   //  printf("\n *(str+4)+1 = %c",*(str+4)+1); // f 

    //  printf("\n *(str+3)+1 = %c",*(str+3)+1);
    //*(str+3)+1
    //*(baseaddress+3) +1
    //b +1 
    // c 

     //printf("\n *str+3+2 = %c",*str+3+2); // x

    return 0;
}
*/

int main(void)
{
    char str[]="Sunbeam"; //S u n b e a m '\0'
    printf("str = %u &str[0] = %u",str,&str[0]);
    printf("\n *str = %s  str[0] = %s",*str,str[0]); 
    //%s is wrong specifier
    // %s is unable to fetch the data 
    // addressing wrong memory location
    // SEGMENTATION FAULT 
    printf("\n str = %s",str);

    return 0;
}


/*
int main(void)
{
    char *str="Sunbeam"; //declaration of string 
    //using character pointer 

    printf("%s",str);
    return 0;
}
*/


/*
int main(void)
{
    char ch[]="A"; //valid declaration
    // char ch[]='A'; // invalid 
    char ch='A'; // valid 
    char ch[]={'A','B','C'}; //valid
    char ch[]={'A'}; //valid 

    return 0;
}
*/

/*
int main(void)
{
    char str[]="PreCAT \0 Sunbeam\0"; 
    printf("Str = %s ",str);
    printf("\n size = %d",sizeof(str));//18

    char str1[10];
    printf("\n Enter some string :");
    scanf("%s",str1);
    printf("\n str1 = %s",str1);
    return 0;
}*/


/*
int main(void)
{
    char str[]="PreCAT\0"; // P r e C A T \0 \0
    printf("Str = %s ",str);
    printf("\n size = %d",sizeof(str));
    return 0;
}
*/


/*
int main(void)
{
    char str[5]="Sunbeam Pune";
    printf("Str = %s ",str);
    printf("\n size = %d",sizeof(str));
   
    return 0;
}*/


/*
int main(void)
{
    char str[]="Sunbeam Pune";
    printf("Str = %s ",str);
    printf("\n size = %d",sizeof(str));
   
    return 0;
}*/



/*
int main(void)
{
    char str[]="Sunbeam";
    printf("Str = %s ",str);
    printf("\n size = %d",sizeof(str));
   
    return 0;
}
*/

/*
int main(void)
{
    char str[20]="PreCAT-OM27"; // PreCAT-OM27 + '\0'
    printf("Str = %s ",str);
    printf("\n size = %d",sizeof(str));
    return 0;
}
*/



/*
int main(void)
{
    char str[]="PreCAT-OM27"; // PreCAT-OM27 + '\0'
    printf("Str = %s ",str);
    printf("\n size = %d",sizeof(str));
    return 0;
}
*/