#include<stdio.h>
void accept_data(int arr[5]);
int main(void)
{
    int a[5],b[5]; //declared two arrays 
    accept_data(a); //function call by passing the array 
    //we always pass name of an array 
    // name of an array always holds the address
    // array address 

    accept_data(b); //function call
    return 0;
}
void accept_data(int arr[5])
{
    printf("Enter Array Elements ");
    for(int i=0;i<5;i++)
        scanf("%d",&arr[i]);
    printf("\n Array Elements are ");
    for(int i=0;i<5;i++)
        printf(" %d ",arr[i]);
    
}





/*
int main(void)
{
    int a[5],b[5];
    int i;
    printf("Enter First Array Elements ");
     for(i=0;i<5;i++)
        scanf("%d",&a[i]); //&a[0]  &a[1]  &a[2] &a[3]  &a[4]


     printf("Enter second Array Elements ");
     for(i=0;i<5;i++)
        scanf("%d",&b[i]); //&b[0]  &b[1]  &b[2] &b[3]  &b[4]


    printf("\n Elements that you have entered  are :");
    for(i=0;i<5;i++)
        printf(" a[%d] =  %d ",i,a[i]);

    printf("\n Elements that you have entered  are :");
    for(i=0;i<5;i++)
        printf(" b[%d] =  %d ",i,b[i]);
    return 0;
}

*/