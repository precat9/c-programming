// copy the contents from source file to destination file

#include<stdio.h>

int main(void)
{
    FILE *fptr1,*fptr2;
    char ch;

    fptr1=fopen("src.txt","r");
    fptr2=fopen("dest.txt","w");
    if(fptr1==NULL)
    {
        printf("file is not found");
        return 0;
    }
    if(fptr2==NULL)
    {
        printf("file is not found");
        return 0;
    }

    ch=fgetc(fptr1); // take first character from fptr1 and store it in ch
    while(ch!=EOF)
    {
        fputc(ch,fptr2);
        ch=fgetc(fptr1);// next character will be fetched
    }
    fclose(fptr1);
    fclose(fptr2);

    printf("\n Contents are written in the destination file");

    return 0;
}







