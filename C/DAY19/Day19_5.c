// file handling


//reading the data from file
//typedef struct _iobuf FILE 
    // there is structure having real name as _iobuf
    // FILE is its nick name 


    //typedef struct Student STUD;
    //there is structucre haveing real name as Student
    // STUD is its nick name
    // struct Student s1; // s1 variable real name
    // STUD s2; // s2 variable nick name
    // STUD *ptr; // ptr is pointer variaabel of type structure 
    //ptr=&s1; 



#include<stdio.h>

int main(void)
{
    FILE *fptr;
    char ch;
    //fptr is pointer variable of type FILE structure 
    fptr=fopen("source.txt","r"); // read mode
    if(fptr==NULL)
    {
        printf("File is not found");
        return 0;
    }
    // every file ends with  EOF (End Of File)
    // EOF = -1  

    // LOGIC PART
       while((ch=fgetc(fptr))!=EOF)
            fputc(ch,stdout); // puts a character on stdout standard output stream
        printf("\n Reading is successful");

        fclose(fptr);
    return 0;
}










