#include<stdio.h>


union Student
{
    int id;
    int rollno;
};

// union memory is shared among all the fields
// allocation is based on who ever is largest data type
int main(void)
{
    union Student s; // s is a variable of type union
    printf("size = %d",sizeof(s));
    s.id=5;
    s.rollno=8;
    printf("\n ID = %d Rollno = %d",s.id,s.rollno);

    return 0;
}



/*union Student
{
    int id;
    int rollno;
};

// union memory is shared among all the fields
// allocation is based on who ever is largest data type
int main(void)
{
    union Student s; // s is a variable of type union
    printf("size = %d",sizeof(s));
    s.id=12;
    printf("\n ID = %d",s.id);
    s.rollno=5;
    printf("\n Rollno = %d",s.rollno);

    return 0;
}*/





/*
struct Student
{
    int id;
    int rollno;
};
//size = 8 bytes 

int main(void)
{
    struct Student s1,s2;
    s1.id=1;
    s1.rollno=1;
    s2.id=2;
    s2.rollno=2;
    printf("s1 = rollno = %d",s1.rollno); // 1
    printf("\n s2 = rollno = %d",s2.rollno); // 2
     printf("\n s1 = id = %d",s1.id); // 1
    printf("\n s2 = id = %d",s2.id); // 2



    return 0;
}

*/