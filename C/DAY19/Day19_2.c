#include<stdio.h>

// we wish to request to the compiler 
// please pack the memory 1 byte every time


#pragma pack(1)

struct Student
{
    int rollno; // 1 + 1 + 1 + 1
    int marks; // 1 + 1 + 1 + 1 
    char grade; // 1 
};

int main(void)
{
   struct Student s;
   printf("Size = %d",sizeof(s));
   printf("\n Size = %d",sizeof(struct Student));
    return 0;
}




/*
//for the structure Student memory packing is done by 4bytes everytime
struct Student
{
    int rollno; // 4bytes
    int marks; //4bytes 
    char grade; //4bytes 
};

int main(void)
{
   struct Student s;
   printf("Size = %d",sizeof(s));
    return 0;
}

*/