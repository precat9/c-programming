
//fseek() move the cursor(file pointer) in the file
// moving the cursor from begining
// moving the cursor from current location
//moving the cursor from end

//ftell()

#include<stdio.h>
int main(void)
{
    FILE *fptr;
    fptr=fopen("demo.txt","r");
    if(fptr==NULL)
    {
        printf("File is not found");
        return 0;
    }

    //fseek(fptr,10,SEEK_SET);
    //printf(" FTELL = %u FGETC = %c ",ftell(fptr),fgetc(fptr));

    //fseek(fptr,5,SEEK_CUR);
    //printf(" \n FTELL = %u FGETC = %c ",ftell(fptr),fgetc(fptr));

    fseek(fptr,-5,SEEK_END);
    printf(" \n FTELL = %u FGETC = %c ",ftell(fptr),fgetc(fptr));

    return 0;
}