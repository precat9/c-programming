//function pointer  
#include<stdio.h>
void get(); // function declaration 
void disp(); // function declaration 

// if we wish to store the address of function
// FUNCTION POINTER 

void (*fnptr)(); // declaration of function pointer 

int main(void)
{
   // get();//function call 
    //disp();
    printf("\n Address of get function = %u ",get);
    printf("\n Address of disp function = %u ",disp);
    fnptr=get; // function pointer is holding address of get function 
    printf("\n Address of function pointer = %u ",fnptr);
    (*fnptr)(); //function pointer call 
    fnptr=disp; // function pointer is holding the address of disp function
    printf("\n Address of function pointer = %u ",fnptr);
    (*fnptr)(); //function pointer call 

    return 0;
}

void get() // function definition 
{
    printf("\n Inside get function");
}

void disp()// function definition 
{
    printf("\n inside display function");
}