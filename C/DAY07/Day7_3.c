#include<stdio.h>

int main(void)
{
    int x=2,y=5;
    (x & y)?printf(" True "):printf(" False ");
    // x  = 2   0 0 1 0
    // y  = 5   0 1 0 1
    //======================
    //          0 0 0 0 ==> 0 
    (x && y)?printf(" True "):printf(" False ");
    //(2 && 5 )
    //(E1 && E2)
    // 1  && 1
    // 1 
    return 0;
}



/*
int main(void)
{
    int num1;
    int num2;
    int min;
    printf("Enter Num1 :");
    scanf("%d",&num1);
    printf("Enter Num2 :");
    scanf("%d",&num2);
    printf("Num1 = %d Num2 = %d",num1,num2);
    //ternary operator
    min = (num1<num2)?num1 : num2;
    printf("\n Minimum Value = %d",min);

    return 0;
}
*/



/*
//ternary operator 
// condition ? true : false 

int main(void)
{
    int num1;
    int num2;
    int max;
    printf("Enter Num1 :");
    scanf("%d",&num1);
    printf("Enter Num2 :");
    scanf("%d",&num2);
    printf("Num1 = %d Num2 = %d",num1,num2);
    //ternary operator
    max = (num1>num2)?num1 : num2;
    printf("\n Maximum Value = %d",max);

    return 0;
}

*/