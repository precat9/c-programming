#include<stdio.h>


// any number is divisble by 5
// num = 30 
// num % 5 ==0 YES
// num =12 
//12 % 5 = 2 NO 


int main(void)
{
   int num;
   printf("Enter Number :");
   scanf("%d",&num);
   if(num>0)
   {
      printf("It is positive number");
   }
   else
    {
       printf("It is negative number ");
    }
   return 0;
}



/*
int main(void)
{
   int num;
   printf("Enter Number :");
   scanf("%d",&num);
   if(num%2==0)
   {
      printf("You are inside if block");
      printf("\n the number you have entered is even number ");
   } //if block or if scope 
   else
   {
      printf("You are inside else block");
      printf("\n the number you have entered is odd number ");
   }
   
   return 0;
}
*/



/*
int main(void)
{
   int num;
   printf("Enter Number :");
   scanf("%d",&num);
   // if the condition inside the if block is true it executes the line  
   // printf("Even number"); and if condition inside the if block is false
   //it will executer else part.  printf("ODD Number");
   if(num%2==0)  
      printf("Even number"); // if block 
   else
      printf("ODD Number"); // else block 
   
   return 0;
}
*/


/*
// check entered number is even or not 
int main(void)
{
   int num;
   printf("Enter Number :");
   scanf("%d",&num);
   if(num%2==0)  
      printf("Even number");
   return 0;
}
*/


// (num%2 == 0)
// num = 5
// (5%2 == 0)
// (1 == 0 )
// False 


//// (num%2 == 0)
// num = 4
// (4 % 2 == 0)
// (0 == 0 ) 
// True 