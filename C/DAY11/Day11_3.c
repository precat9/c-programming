#include<stdio.h>

void swap(int a,int b); 


int main()
{
    int a,b;
    printf("Enter First Number ");
    scanf("%d",&a);
     printf("Enter Second Number ");
    scanf("%d",&b);
    printf("\n Before Swap :  A = %d B = %d",a,b);
    swap(a,b); //function call // call by value 
    //swap(a) ; //NOT ALLOWED // function is expecting two arguemnts

    printf("\n After Swap : A = %d B = %d",a,b);
    return 0;
}
void swap(int n1,int n2)
{
    printf("\n Inside Swap : Before Swap :  N1 = %d N2 = %d",n1,n2);
    n1=n1+n2;
    n2=n1-n2; 
    n1=n1-n2; 
  printf("\n Inside Swap : After Swap :  N1 = %d N2 = %d",n1,n2);
   
}


//swap of two numbers 
// n1 = 10 n2 = 5
// after swap
// n1 = 5  n2 = 10 

// Logic
// int temp;  
// temp = n1;  // temp = 10
// n1= n2;     // n1 = 5 
// n2= temp;  // n2= 10

//Logic
// n1=n1+n2;   // n1 = 5+10  // n1=15
// n2=n1-n2;  // n2 = 15-10  // n2 = 5
// n1=n1-n2;  // n1 = 15-5  // n1 = 10 


