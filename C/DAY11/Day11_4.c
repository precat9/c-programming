#include<stdio.h>
/*int main()
{
char ch;  //local 
//auto signed char ch;
return 0;  
}*/

int main(void)
{
    auto int val;
    // int val ; //same as above line
   // val=10;
    printf("value of val = %d",val);
    return 0;
} // block / scope  of main() 
//printf("val = %d", val); // can not access val because it is local variabel of main 

// val is a variable of type int
//  val is declared within main() function
// val is declared within main() scope
// val variable is having a Scope of main() 
// Life of val variable is also scope of main() only
//val is a local variable within main() function
// All local variables recevies the memory from STACK SECTION
//all local variables receives by default auto keyword
//auto automatic
