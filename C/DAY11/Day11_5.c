/*#include<stdio.h>
void demo();
int main()
{
   demo(); //call
    demo();
    demo();
    return 0;
}

void demo() 
{
    int num=30; //auto variable (local) // stack area 
   
    static int val; // static variable declaration
    val=num; // we can not assign another variable value to static variables
    // we have to compulsory initialize it at the time of declaration

    printf("\n Num = %d Val = %d",num,val);
    num++;
    val++;
    printf(" After Increment Num = %d Val = %d",num,val);
}
 */
//printf("val = %d",val); // Not allowed 

/*
void demo();
int main()
{
   demo(); //call
    demo();
    demo();
    return 0;
}

void demo() 
{
    int num=30; //auto variable (local) // stack area 
    //Rule: static variables must be compulsorily initialized 
    //at the time of declrataion
    //if you assign value to static variable after declaration
    //it will be treated as local variables by the compiler 
    static int val; // static variable declaration
    val=50; 
    printf("\n Num = %d Val = %d",num,val);
    num++;
    val++;
    printf(" After Increment Num = %d Val = %d",num,val);
}
 
*/




#include<stdio.h>
void demo();// declaration

int main(void)
{
    demo(); //call
    demo();
    demo();

   
    return 0;
}
//static variables gets intialized only once thorugh out the life cycle of the program
// it always maintains the last updated state of the data 


void demo() //definition 
{
    int num=30; //auto variable (local) // stack area 
    static int val=50; // static variables receives memory from DATA SECTION
    printf("\n Num = %d Val = %d",num,val);
    num++;
    val++;
    printf(" After Increment Num = %d Val = %d",num,val);
}



/*
#include<stdio.h>
void demo();// declaration

int main(void)
{
    demo(); //call
    //demo();
    //demo();
    return 0;
}

void demo() //definition 
{
    int num=30; //auto variable (local) // stack area 

    printf("\n Num = %d",num);
    num++;
    printf(" After Increment Num = %d",num);
}

*/