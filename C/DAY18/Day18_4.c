#include<stdio.h>

//Array of Structure 

struct Employee
{
    int id;
    float salary;
    char name[30];
    int age;
};

int main(void)
{
    int i;
    struct Employee e[2]; //  e is an array of structure of type Employee
    for(i=0;i<2;i++)
    {
        printf("Enter Empid:");
        scanf("%d",&e[i].id);
         printf("\n enter Salary :");
         scanf("%f",&e[i].salary);
        printf("\n Enter Name :");
        scanf("%s",e[i].name);
        printf("\n Enter Age :");
        scanf("%d",&e[i].age);
    }
    printf("\n Data That you have entered is as follows : \n");
    for(i=0;i<2;i++)
    {
    printf("\n ID = %d Salary = %f Name = %s Age = %d",e[i].id,e[i].salary,e[i].name,e[i].age);
    }
    return 0;
}



/*
struct Employee
{
    char grade;
    int empid;
    float salary;
    char name[30];
    int age;
};

int main(void)
{
    struct Employee e1; // e1 is a variable of type Employee
    printf("Enter Grade :");
    scanf("%c",&e1.grade);
    printf("Enter Empid:");
    scanf("%d",&e1.empid);
    printf("\n enter Salary :");
    scanf("%f",&e1.salary);
    printf("\n Enter Name :");
    scanf("%s",e1.name);
    printf("\n Enter Age :");
    scanf("%d",&e1.age);
    printf("\n Name = %s Age = %d",e1.name,e1.age);
    printf(" \n Grade = %c ID = %d Salary = %f ",e1.grade,e1.empid,e1.salary);

    return 0;
}

*/