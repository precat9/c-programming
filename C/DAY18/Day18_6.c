#include<stdio.h>
#include<string.h>

struct Student
{
    int rollno;
    char name[20];
    int marks;
};

int main(void)
{
    struct Student s;
    s.rollno=1;
    strcpy(s.name,"Akshita");
    s.marks=85;
    printf("RN = %d Name = %s Marks = %d",s.rollno,s.name,s.marks);

    struct Student *ptr=&s;
    printf("\n Accessing the elements with the help of pointer");
    printf("\nRN = %d Name = %s Marks = %d",ptr->rollno,ptr->name,ptr->marks);

    return 0;
}

