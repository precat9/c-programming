#include<stdio.h>

//Nested Structure 
struct Student
{
    int rollno;
    int age;
    struct 
    {
        int dd;
        int mm;
        int yy;
    }dob;   
};

int main(void)
{
    struct Student s1;
    s1.age=34;
    s1.rollno=1;
    s1.dob.dd=14;
    s1.dob.mm=12;
    s1.dob.yy=2021;
    printf("Age = %d Roll no = %d",s1.age,s1.rollno);
    printf("\n DD-MM-YY : %d-%d-%d",s1.dob.dd,s1.dob.mm,s1.dob.yy);
    return 0;
}