#include<stdio.h>
#include<stdlib.h>

//dynamic memory allocation is done in heap section

//receive the memory at run time then we go for dynamic memory allocation

int main(void)
{
    int n;
    int *ptr=NULL;
    int i;
    printf("Enter Number of elements you wish to insert in an array");
    scanf("%d",&n);

    //1. Request for the memory

           //ptr = (int*)malloc(sizeof(int)*n);
           // malloc() is having return type as void ptr
           // in case of void ptr while dereferencing the data
           // we typecast at
           
           ptr=(int *)calloc(n,sizeof(int));

           // malloc allocated memory receives by default garbage value
           // calloc allocated memory receives by default 0 as initial values  

           if(ptr==NULL)    
           {
            printf("Memory can not be allocated");
            return 0;
           }
    //2. Use that memory
    printf("\n Enter Array Elements ");
    for(i=0;i<n;i++)
    {
        scanf("%d",&ptr[i]);
    }

    printf("\n Array Elements are :\n");
    for(i=0;i<n;i++)
    {
        printf("   %d  ",ptr[i]);
    }
    //3. Release the memory
    free(ptr);
    ptr=NULL;
    
    return 0;
}

