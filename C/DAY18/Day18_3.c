#include<stdio.h>
struct Student
{
    int rollno;
    int marks;
    char grade;
}s1={1,86,'A'}; // initialization of structure variable at the time of declaration

int main(void)
{
    printf("Rollno : %d MArks = %d Grade : %c",s1.rollno,s1.marks,s1.grade);
    struct Student s2;
    s2.rollno=s1.rollno; // copy the data element by element 
     s2.marks=s1.marks;
     s2.grade=s1.grade;
    printf("\n S2 Rollno : %d MArks = %d Grade : %c",s2.rollno,s2.marks,s2.grade);
    
    struct Student s3; // s3 variable
    s3=s2; // direct copy of one structure variable into another structure variabel
     printf("\n S3 Rollno : %d MArks = %d Grade : %c",s3.rollno,s3.marks,s3.grade);
    
    return 0;
}

/*
struct Student
{
    int rollno;
    int marks;
    char grade;
}s1,s2; // declaration of structure variable at the time of defining a structure 

int main(void)
{
    s1.rollno=1;
    s2.rollno=2;

    return 0;
}
*/

/*
struct Student
{
    int rollno;
    int marks;
    char grade;
}; 
int main(void)
{
    struct Student s1,s2,s3; // three structure variables  declaration
    return 0;
}
*/



/*
struct Student
{
    int rollno;
    int marks;
    char grade;
}; 

int main(void)
{
    // access the data members of structure we declare a variable
    struct Student s1; // s1 is a variable of structure type 
    s1.rollno=1;
    s1.marks=85;
    s1.grade='A';
    printf("S1 : Rollno = %d Marks = %d Grade = %c",s1.rollno,s1.marks,s1.grade);
    struct Student s2; // s2 is variable 
    s2.rollno=2;
    s2.marks=90;
    s2.grade='A';
    printf("\n S2 : Rollno = %d Marks = %d Grade = %c",s2.rollno,s2.marks,s2.grade);


    return 0;
}

*/