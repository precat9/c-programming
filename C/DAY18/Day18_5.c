#include<stdio.h>


typedef struct // anonymous structure 
{
    int rollno;
    int age;
}EMP; // EMP is nick name 
 
 // anonymous structure can  have only nick name also 


int main(void)
{
    EMP e1; // EMP is nick name e1 is variable name 
    e1.rollno=10;
    e1.age=50;
    printf("%d",e1.rollno);
    return 0;
}


/*
struct // anonymous structure 
{
    int rollno;
    int age;
}e1; // variable declare 


int main(void)
{
    //struct--e1; // if we have anonymous structure we can not create variable inside main
    e1.age=35;
    e1.rollno=5;
     printf("%d",e1.rollno);
    return 0;
}
*/

/*
typedef struct Employee 
{
    int id;
    int salary;
}EMP;
// Real name : struct Employee
// Nick name : EMP 

int main(void)
{
    struct Employee e1; //e1 is a variabel created using real name
    EMP e2; //e2 is a variable of type Employee created using nick name 
    e1.id=1;
    e1.salary=50000;
    e2.id=2;
    e2.salary=70000;


    return 0;
}

*/