#include<stdio.h>



extern int a; //variable a is defined externally for this program // Data section


int main(void)
{
    
    printf("A = %d",a);
    return 0;
}
a=50;






/*
int num=50; // global variable 

int main(void)
{
    int num=45;// local variable
    printf("Num = %d",num); 
    // LOCAL VARIABLES HAVE MORE PRIORITY THAN GLOBAL
    //WHEN LOCAL AND GLOBAL VARIABLE NAMES ARE SAME 
    return 0;
}

*/

/*
register int a=45;  // we can not declare register in global area
int main(void)
{
    return 0;
}
*/


/*
int a; //global variable decalration // DATA SECTION 
int main(void)
{
    a=50;
    printf("A = %d",a);
    return 0;
}

*/


/*
//address operator (& ) can not be used with registers
int main(void)
{
    register int var=40; // recommended
    printf("Var = %d",var); //allowed
    printf("Enter regsiter variable value ");
    scanf("%d",&var); // NOT ALLOWED
     //Compile time error: address of register variable 'var' requested
    return 0;
}
*/



/*
int main(void)
{
    int num;
    printf("Enter Num ");
    scanf("%d",&num);
    printf("Number = %d Location = %d",num,&num);
    return 0;
}

*/

/*
//if we wish to store some variables inside CPU registers
//then we use register keyword

int main(void)
{
    register int num=50;
    // requesting to compiler 
    // allocate the memory for num in CPU registers 

    //1. if CPU register is available
    // Operting system will allocate memory inside CPU register
    // Programmer can enjoy the speedy performance 

    //2. if cpu register is not available
    // every time when we try to execute the program
    //every time OS will keep on checking the 
    //availibilty of CPU register //which intern wastes the time 

    printf("Num = %d",num);
    return 0;
}

*/