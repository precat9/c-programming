#include<stdio.h>

/*
int main(void)
{
    int a=10;
    printf("%d",a);
    main();  //recursive  call
    // NEVER RECOMENDED without any termination condition
    //stack overflow run time error
    return 0;
}

*/




int fact_num(int num); //function declaration

int main(void)
{
    int val;
    int result;
    printf("Enter Number :");
    scanf("%d",&val);
    result = fact_num(val);
    printf("\n Factorial of num = %d",result);
    return 0;
}


int fact_num(int num)
{
    if(num==1)
        return 1;
    else
        return num * fact_num(num-1);
}