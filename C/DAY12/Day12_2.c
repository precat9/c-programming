#include<stdio.h>

int fact_num(int num); //function declaration

int main(void)
{
    int val;
    int result;
    printf("Enter Number :");
    scanf("%d",&val);
    result = fact_num(val);
    printf("\n Factorial of num = %d",result);
    return 0;
}

int fact_num(int num)
{
    //logic for factorial 
    int ans=1;
    while(num)
    {
        ans=ans*num;
        num--;
    }
    return ans;
}