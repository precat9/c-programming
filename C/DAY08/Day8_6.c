#include<stdio.h>



/*int main(void)
{
    int i=20;
    while(i>=1) 
    {
        i-=2;
        printf(" %d ",i); 
       
    }
    return 0;
}
*/



/*
int main(void)
{
    int i=20;
    while(i>=10) 
    {
        printf(" %d ",i); 
       // i--;
        i-=2;     //i= i -2 
    }
    return 0;
}
*/


/*
int main(void)
{
    int i=5;
    while(i<=10) // 5<=10 True // 5<=10 True //5<=10 True 
    {
        printf(" %d ",i);  // 5  5 5 ..... INFINITE LOOP 
        // because we have not modified the looping variable
        // by writing either incr or decr operation 
        // Looping Variable modification helps us to reach to the terminating condition 
    }
    return 0;
}

*/



/*
int main(void)
{
    int i=1; // intial state for the variable i 
    // i is the looping variable 
    while(i<=5) // i<=5 Terminating condition 
    {
        printf(" %d ",i);
       // i++; // increment operation on looping variable 
       ++i; // OK 
    }
    printf("\n value of i outside while loop %d",i);

    return 0;
}

*/