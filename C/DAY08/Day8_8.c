#include<stdio.h>

// rows in columns 
// nested for loops 
// for loop within another for loop is called as nested for loop
int main(void)
{
    int i,j;
    for(i=1;i<=3;i++) // Outer Rows 
    {
        for(j=1;j<=2;j++) // Inner Cols 
        {
            printf(" %d %d ",i,j);
        }
        printf("\n");
    }
    return 0;
}