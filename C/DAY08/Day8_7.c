#include<stdio.h>


/*
int main(void)
{
    int i;
    for(i=1;i<=10;i++)
    {
        printf(" \n 3 * %d = %d  ",i,3*i);

    }
    return 0;
}

*/


/*
int main(void)
{
    int k;
    for(k=1;k<=20;)   //19<=20 ??? TRUE  //21 < =20 FALSE 
    {
        k+=2; //k = k+2 // 19+2 = 21 
        printf(" %d ",k); // 21 
    }
    return 0;
}
*/



/*
int main(void)
{
    int k;
    for(k=1;k<=20;k+=2)  // Odd numbers  
    {
        printf(" %d ",k);
    }
    return 0;
}
*/


/*
int main(void)
{
    int k;
    for(k=0;k<=20;k+=2)  // Even numbers  
    {
        printf(" %d ",k);
    }
    return 0;
}
*/

/*
int main(void)
{
    int k=1;
    for(;k<=20;)  // VALID Syntax 
    {
        
        printf(" %d ",k);
        k++;
    }
    return 0;
}
*/

/*
int main(void)
{
    int k=1;
    for(;k<=20;k++) // writing of initial state of loop variable inside for loop is optional
    // provided you have initialized it prior to for loop
    {
        printf(" %d ",k);
    }
    return 0;
}
*/


/*
// for loop
// for( ____ ; ____ ; _____)
// First : initialization state of looping variable 
// Second : conditional statement 
// Third : incr/decrement operation

int main(void)
{
    int i; // looping variable 
    for(i=0;i<=10;i++)
    {
        printf(" %d ",i);
    }
    return 0;
}

*/