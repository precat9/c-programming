#include<stdio.h>


int main(void)
{
    int n1,n2;
    int choice;
    printf("Enter First Number ");
    scanf("%d",&n1);
    printf("Enter Second Numeber :");
    scanf("%d",&n2);
    printf("Enter Choice : 1.Add 2.Sub 3.Mul 4.Div 5.Mod");
    scanf("%d",&choice);
    switch(choice)
    {
        case 'A': // this case is considered internally as case 65:
        printf("Addition = %d",n1+n2);
        break;

        case 2:
         printf("Subtraction = %d",n1-n2);
        break;
        
        case 3:
         printf("Multiplication = %d",n1*n2);
        break;

        case 4:
         printf("Division = %d",n1/n2);
        break;

        case 5:
         printf("Mod = %d",n1%n2);
        break;
        
    }

    return 0;
}




/*
int main(void)
{
    int n1,n2;
    int choice;
    printf("Enter First Number ");
    scanf("%d",&n1);
    printf("Enter Second Numeber :");
    scanf("%d",&n2);
    printf("Enter Choice : 1.Add 2.Sub 3.Mul 4.Div 5.Mod");
    scanf("%d",&choice);
    switch(choice)
    {
        // DUPLICATION OF CASE IS NOT ALLOWED

        case 1:
        printf("Addition = %d",n1+n2);
        break;

        case 1 : //error: duplicate case value (compile time error)
        printf("N1 = %d N2 = %d N1+n2 = %d",n1,n2,n1+n2);
        break;

        case 2:
         printf("Subtraction = %d",n1-n2);
        break;
        
        case 3:
         printf("Multiplication = %d",n1*n2);
        break;

        case 4:
         printf("Division = %d",n1/n2);
        break;

        case 5:
         printf("Mod = %d",n1%n2);
        break;
        
    }

    return 0;
}
*/


/*
int main(void)
{
    int n1,n2;
    int choice;
    printf("Enter First Number ");
    scanf("%d",&n1);
    printf("Enter Second Numeber :");
    scanf("%d",&n2);
    printf("Enter Choice : 1.Add 2.Sub 3.Mul 4.Div 5.Mod");
    scanf("%d",&choice);
    switch(choice)
    {
        //SEQUENCE OF SWITCH CASE DOEN NOT MATTER 
        case 2:
         printf("Subtraction = %d",n1-n2);
        break;

        case 1:
        printf("Addition = %d",n1+n2);
        break;
        
        
        case 3:
         printf("Multiplication = %d",n1*n2);
        break;

        case 4:
         printf("Division = %d",n1/n2);
        break;

        case 5:
         printf("Mod = %d",n1%n2);
        break;
        
    }

    return 0;
}
*/


/*
int main(void)
{
    int n1,n2;
    int choice;
    printf("Enter First Number ");
    scanf("%d",&n1);
    printf("Enter Second Numeber :");
    scanf("%d",&n2);
    printf("Enter Choice : 1.Add 2.Sub 3.Mul 4.Div 5.Mod");
    scanf("%d",&choice);
    switch(choice)
    {
        case 1:
        printf("Addition = %d",n1+n2);
        break;
        
        case 2:
         printf("Subtraction = %d",n1-n2);
        break;

        case 3:
         printf("Multiplication = %d",n1*n2);
        break;

        case 4:
         printf("Division = %d",n1/n2);
        break;

        case 5:
         printf("Mod = %d",n1%n2);
        break;
        // writing of default is optional 
        //default:
           // printf("You have entered wrong option");
    }

    return 0;
}

*/