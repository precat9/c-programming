#include<stdio.h>
//choice based input  : switch case block 

// 1 (n1+n2)
// 2 (n1-n2)
// 3 (n1*n2)
// 4 (n1/n2)
// 5 (n1%n2)
// otherwise wrong option 


int main(void)
{
    int n1,n2;
    int choice;
    printf("Enter First Number ");
    scanf("%d",&n1);
    printf("Enter Second Numeber :");
    scanf("%d",&n2);
    printf("Enter Choice : 1.Add 2.Sub 3.Mul 4.Div 5.Mod");
    scanf("%d",&choice);
    switch(choice)
    {
        case 1:
        printf("Addition = %d",n1+n2);
        break;
        
        case 2:
         printf("Subtraction = %d",n1-n2);
        break;

        case 3:
         printf("Multiplication = %d",n1*n2);
        break;

        case 4:
         printf("Division = %d",n1/n2);
        break;

        case 5:
         printf("Mod = %d",n1%n2);
        break;

        default:
            printf("You have entered wrong option");
    }

    return 0;
}