#include<stdio.h>


int main(void)
{
    int num=50; // num is a variable created using real name
    typedef int NUMBER; // int is real name and NUMBER is nick name
    //NUMBER val = 55; // val is a variable created using nick name 
    int val = 55; // because NUMBER is having real name as int

    printf("Num = %d",num);
    printf("\n Val = %d",val);

    return 0;
}