/*#include<stdio.h>



// 1 . Tea
// 2 . Coffee
// 3 . Exit

int main(void)
{
    int choice;
    do
    {
    printf("\n Enter Choice : 0.Exit 1. Tea 2. Coffee \n ");
    scanf("%d",&choice);
    switch(choice)
    {
        case 0:
        break;
        case 1:
        printf("you have ordered Tea");
        break;
        case 2:
        printf("you have ordered coffee");
        break;
    }
    }while(choice!=0);
    return 0;

}*/






//option based program ===> switch...case
// in continuation manner
// Menu Driven Program 
// do...while loop 
#include<stdio.h>
int main(void)
{
    int n1,n2;
    int choice;
    printf("Enter First Number ");
    scanf("%d",&n1);
    printf("Enter Second Numeber :");
    scanf("%d",&n2);
    do
    {
    printf("\n Please select Choice : 0.Exit  1.Add  2.Sub  3.Mul  4.Div  5.Mod \n");
    scanf("%d",&choice);
    switch(choice)
    {
        case 1:
        printf("Addition = %d",n1+n2);
        break;
        
        case 2:
         printf("Subtraction = %d",n1-n2);
        break;

        case 3:
         printf("Multiplication = %d",n1*n2);
        break;

        case 4:
         printf("Division = %d",n1/n2);
        break;

        case 5:
         printf("Mod = %d",n1%n2);
        break;
    }
    } while(choice!=0); // 1!=0 True 2!=0 True 3!=0 True 4!=0 treu 
    //0!=0 false 
    // do while loop will first performs the operation
    //then it checks the condition of while loop

    return 0;
}


