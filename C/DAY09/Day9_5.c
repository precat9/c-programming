#include<stdio.h>

/*
//enum colors{RED,BLUE,ORANGE,RED};// NOT ALLOWED ERROR 
enum colors{RED,BLUE,ORANGE};
int main(void)
{
    printf("%d",BLUE+ORANGE); // 1 + 2 
    // RED=ORANGE; // RED = 2  // NOT ALLOWED // LVALUE ERROR 
   // RED++;  // RED = RED + 1 // NOT ALLOWED // LVALUE ERROR 
    //RED=BLUE+ORANGE; // NO 

    int i;
    i = RED + ORANGE; //ALLOWED 
    return 0;
}
*/
/* 
enum colors{white,blue,green};
//white = 0
// blue = 1
// green = 2
int main(void)
{
    printf("enter the value of white color");
    scanf("%d",&white); // lvalue : Location Value error 
    //compile time error 
    return 0;
}
*/




/*
//enum colors{RED,GREEN,BLUE}; // 0 1 2 
//enum colors{RED=10,GREEN=20,BLUE=30}; // 10 20 30 
//enum colors{RED=50,GREEN=20,BLUE=70}; // 50 20 70
//enum colors{RED,GREEN=20,BLUE=70}; // 0 20 70 

//enum colors{RED=22,GREEN,BLUE}; // 22  23   24  
// if value is not assigned to enum element
//then next value is step one ahead of previous value 

// enum colors{RED=10,GREEN,BLUE=15}; // 10   11   15
enum colors{RED=10,GREEN,BLUE=11};   //10  11  11 (not recommended)
int main(void)
{
    printf("Red = %d Green = %d Blue = %d",RED,GREEN,BLUE);

    return 0;
}

*/

/*
enum colors{BLACK,RED,GREEN,BLUE,ORANGE};
// 0 1 2 3 4 
*/

/*
enum Department{ADMIN,TRAINING,SECURITY,PLACEMENT};
// enum Department is a user defined datatype
// ADMIN  = 0 
// TRAINING = 1
//SECURITY = 2
//PLACEMENT  = 3 

*/

/*
// enum
// enumeration 
// it is used for declaring some set of constants in the program 
//enum is always in the form of integers 
// enum is generally declared outside the main 

enum colors{RED,GREEN,BLUE};
// enum is a keyword , it is user defined datatype
// colors is the name given to set of elements 
// RED, GREEN,BLUE are three constants inside colors set
// where
// RED = 0
// GREEN = 1
// BLUE = 2

int main(void)
{
    printf(" red = %d ",RED);
     printf(" green = %d ",GREEN);
     printf(" blue = %d ",BLUE);
    
    return 0;
}

*/