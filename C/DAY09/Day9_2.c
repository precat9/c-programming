#include<stdio.h>

/*
int main(void)
{
    int i;
    for(i=0;i<10;i++) // i = 0 upto 9 
    {
        
        if(i==5)
            continue;  // pls continue the loop iteration
            //directly continue to the next iteration of for loop 
        printf(" %d ",i);
    }
    printf("\n Outside the for loop");
    return 0;
}

*/

/*
int main(void)
{
    int i;
    for(i=0;i<10;i++) // i =0 upto 9 
    {
        printf(" %d ",i);
        if(i==5)
            break;  // due to this break loop will break after i=5
        
    }
    printf("\n Outside the for loop");
    return 0;
}

*/



/*
int main(void)
{
    int num;
    printf("Enter number :");
    scanf("%d",&num);
    if(num==5)
    {
        goto L1; //L1 label name  //goto Labelname; 
        printf("Inside if block");
        printf("Num = %d",num);
        L1:printf("jumping to this statement");

        //we avoid using goto because
        // sometimes once the control of the program
        //goes to label.
        // in between statements gets skipped 
    }
   
    

    return 0;
}

*/