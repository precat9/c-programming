#include<stdio.h>

//application of enum
// it increases the code readability

enum options{EXIT,ADD,SUB,MUL,DIV,MOD};

int main(void)
{
    int n1,n2;
    int choice;
    printf("Enter First Number ");
    scanf("%d",&n1);
    printf("Enter Second Numeber :");
    scanf("%d",&n2);
    do
    {
    printf("\n Please select Choice : 0.Exit  1.Add  2.Sub  3.Mul  4.Div  5.Mod \n");
    scanf("%d",&choice);
    switch(choice)
    {
        case ADD:
        printf("Addition = %d",n1+n2);
        break;
        
        case SUB:
         printf("Subtraction = %d",n1-n2);
        break;

        case MUL:
         printf("Multiplication = %d",n1*n2);
        break;

        case DIV:
         printf("Division = %d",n1/n2);
        break;

        case MOD:
         printf("Mod = %d",n1%n2);
        break;
    }
    } while(choice!=EXIT); // 1!=0 True 2!=0 True 3!=0 True 4!=0 treu 
    //0!=0 false 
    // do while loop will first performs the operation
    //then it checks the condition of while loop

    return 0;
}
