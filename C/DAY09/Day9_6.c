#include<stdio.h>

enum PERSON{EMPLOYEE,MANAGER,SECURITY};
int main(void)
{
    printf("Security = %d ",SECURITY);
    enum PERSON val; // val is a varaiable of type enum PERSON
    val = MANAGER;
    
    printf("\n Val = %d",val);
    // enum variable can point to one single element at a time
    printf("\n size of val = %d",sizeof(val));
    printf("\n size of enum = %d",sizeof(enum PERSON));

    return 0;
}



/*
// if we wish to access enum elements
//1. Direct way ADMIN ....
//2. store the element in a varaible of enum type and then access it 
enum DEPARTMENT{ADMIN,TRAINING,PLACEMENT};
// enum DEPARTMENT is a user defined datatype


int main(void)
{
    //datatype variablename; 
    enum DEPARTMENT d; // d is a variable of type enum DEPARTMENT
    //printf("%d",PLACEMENT); // 2 (1st way)
    d=PLACEMENT;  //2nd Way // assigning enum variable with its element
    //d is a variable assigned with PLACEMENT as a element 
    // d = 2 
    printf("value of variable d = %d",d);

    return 0;
}

*/
// int a; // int is datatype predefined
// datatype variable name; 