#include<stdio.h>


int main(void)
{
    int a[5]={10,20,30,40,50};
    printf("Address : %u",a); // 280 Assuming base address 
    int i;
    for(i=0;i<5;i++)
        printf(" \n %d at %u ",a[i],&a[i]);

    printf("\n a = %u *a = %d",a,*a);
    // a = 280  *a = *280 = 10 
    printf("\n a+1 = %u *(a+1) = %d",a+1,*(a+1));
    //a+1           //*(a+1)
    // 280 + 1      //*(280+1)
    // 284         ///* (284) =20 

    printf("\n a+2 = %u *(a+2) = %d",a+2,*(a+2));
    //a+2           //*(a+2)
    // 280 + 2      //*(280+2)
    // 288         /// *(288) =30 

    printf("\n a+3 = %u *(a+3) = %d",a+3,*(a+3));
    //a+3           //*(a+2)
    // 280 + 3      //*(280+3)
    // 292        /// *(292) =40 

    return 0;
}

/*
int main(void)
{
    int arr[4]={10,20,30,40};
    printf(" %d ",arr[1]); //20 
    printf("\n %d",1[arr]); //20 //valid // equivalent to arr[1]
    
    //arr[index] OR  index[arr]

    return 0;
}

*/