#include<stdio.h>


int main(void)
{
    int arr[4]={10,20,30,40};
     // 288    292   296   300
    printf("*arr = %d arr = %u",*arr,arr);
    
    //printf("\n *(arr+1) = %d",*(arr+1));

    // *(288 + 1)
    // *(292) 
    // 20

    //printf("\n *arr+1 = %d",*arr+1);
    //*arr+1
    // *288+1
    // 10 +1
    //11 

    //printf("\n *arr+4 = %d",*arr+4);
    //*arr+4
    //*288 +4
    // 10 + 4
    //14 

    //printf("\n *(arr+1)+1 = %d",*(arr+1)+1);
    //*(arr+1)+1 
    //*(288+1)+1
    //* 292 +1
    // 20   + 1
    //21 


    // printf("\n *(arr+2)+5 = %d",*(arr+2)+5);

   // printf("\n *(arr+3)+5+2 = %d",*(arr+3)+5+2);

   //printf("\n *(arr+2)-5 = %d",*(arr+2)-5);

   //printf("\n *(arr+1)+*(arr+2) = %d",*(arr+1)+*(arr+2));

   printf("\n arr = %u arr + 1 = %u arr-1 = %u  ",arr,arr+1,arr-1);
    //printf("\n arr[1]+arr[2] = %d",arr[1]+arr[2]);

    //printf("%d",*(arr+5-2)); //*(arr+3)


    return 0;
}
