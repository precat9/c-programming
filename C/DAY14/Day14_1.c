#include<stdio.h>

// if we wish to store simple data , we noramally declare variables
// if we wish to store variable address, we declare a pointer
// if we wish to store the address  of pointer, we declare pointer to pointer 


/*void update(int **pp);
int main(void)
{
    int num=50;
    int *ptr=&num;
    printf("\n Num = %d *ptr = %d ",num,*ptr);
    printf("\n &Num = %u ptr = %u ",&num,ptr);
    update(&ptr); //address of pointer 
    printf("\n num = %d",num);
    return 0;

}
void update(int **pp)
{
    printf("\n **pp = %d",**pp);
    **pp=60;
}*/



/*
int main(void)
{
    int num=45;
    printf("Before Num = %d",num);
    change(&num);
    printf("\n After Num = %d",num);

    return 0;
}

void change(int *ptr)
{
    printf("\n value of ptr inside change function %d",*ptr); // 45
    *ptr=55;
    printf("\n after modification %d",*ptr); //55
}

*/


void swap(int *a,int *b);
int main(void)
{
   int n1=5,n2=4;
   printf("Before : N1 = %d N2 = %d",n1,n2); // 5   4 
    swap(&n1,&n2); //calling a function by passing address
    printf("\n After : N1 = %d N2 = %d",n1,n2); // 4  5 
    return 0;
}
void swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;

}





/*void swap(int a,int b);

int main(void)
{
   int n1=5,n2=4;
   printf("Before : N1 = %d N2 = %d",n1,n2); // 5   4 
    swap(n1,n2);
    printf("\n After : N1 = %d N2 = %d",n1,n2); //  5    4 
    return 0;
}

void swap(int a,int b)
{
    int temp;
    temp=a;
    a=b;
    b=temp;
}*/