#include<stdio.h>


int main(void)
{
    int a[4]={11,22,33,44};
    printf("a = %u",a);
    printf("\n &a[0] = %u",&a[0]);
    printf("\n &a = %u",&a);
    printf("\n a+1 = %u &a+1 = %u",a+1,&a+1);
    printf("\n a+2 = %u &a+2 = %u",a+2,&a+2);
    //printf("\n %u",&(a+1)); //error
    // &(a+1)
    // &(288+1)
     //&(292)
      // NO 

    return 0;
}