#include<stdio.h>


int main(void)
{
    int b[5]={1,2,3,4,5,6,7,8,9,10};
     // b[0]  b[1]  b[2] b[3]  b[4]  GARBAGE 
    // 280   284   288   292   296   300   304   308...... 
    int i;
    for(i=0;i<9;i++)
    printf(" %d ",b[i]); // 
    printf("\n address : %u",b);

    return 0;
}



/*
int main(void)
{
    int a[6]={1,2,3}; // 1   2   3   0   0  0 
    printf("size = %d",sizeof(a)); // 6elements  * 4bytes each =24 bytes 

    int b[5]={10,20,30,40,50,60}; //warning  
    // giving size of array is job of developer
    printf("\n size of b array= %d",sizeof(b));


    return 0;
}
*/



/*int main(void)
{
    int i;
    int arr[4]={10,20,30,40};
    for(i=0;i<4;i++)
        printf(" \n  %d  %u",arr[i],&arr[i]);
    printf("\n size of an array = %d",sizeof(arr));
    printf("\n size = %d",sizeof(arr[1])); // 4bytes 
    return 0;
}*/





/*
int main(void)
{
    int arr[5]; //declaration
    // 55   65   75   GARBAGE GARBAGE 
    //Rule:
    // if we declare array and if we assign values partially
    // after the declaration on the next line (later stage )
    //remaining elements will receive garbage values 
    int i;
    arr[0]=55;
    arr[1]=65;
    arr[2] = 75;
    for(i=0;i<5;i++)   //i = 0 ,1, 2, 3, 4 
        printf("arr[%d] =  %d ",i,arr[i]); 

    return 0;
}

*/



/*
int main(void)
{
    int i;
    int a[6]={11,22,33};  // array can store 6 elements (24 bytes)
    // 11   22   33   0   0   0
    //Rule:
    // if we initialize array partially
    // remaining elements will receive 0 as its initial value 
    for(i=0;i<6;i++)   //i = 0 ,1, 2, 3, 4 ,5 
        printf("a[%d] =  %d ",i,a[i]); 
    return 0;
}


*/




/*
int main(void)
{
    int a[]={11,22,33,44}; //valid
    // if we don't specify the size of an array
    // it is compulsory to specify the elements within an array on same line

    return 0;
}
*/


/*
int main(void)
{
    int b[];  //compile time error: array size missing in 'b'
    return 0;
}
*/




/*
int main(void)
{
    int a[5]={10,20,30,40,50}; // 0 to 4 
    // array initialization
    int i;
    for(i=0;i<5;i++)   //i = 0 ,1, 2, 3, 4 
        printf("a[%d] =  %d ",i,a[i]); // a[0] a[1] a[2] a[3] a[4] 
    return 0;
}
*/

/*
int main(void)
{
    int i;
    int arr[4];  //declaration of an array
      // arr is name of an array // local // auto // stack 
    arr[0]=10; // assigning the values to array elements 
    arr[1]=20;
    arr[2]=30;
    arr[3]=40;

    for(i=0;i<4;i++)
        printf(" %d ",arr[i]); //i =0 arr[0] //i=1 arr[1] i=2   arr[2]  i=3 arr[3] 
    
    return 0;
}

*/