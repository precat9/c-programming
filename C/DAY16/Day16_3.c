#include<stdio.h>
#include<string.h>


//finding a character in a string (strchr)
/*int main(void)
{
    char s1[10];
   char ch;
    char *ptr=NULL;
    printf("Enter String :");
    scanf("%s",s1);
    printf("Enter Character to be searched ");
    scanf("%*c"); // skip the meaning of new line character
    scanf("%c",&ch);
    ptr=strchr(s1,ch); 
    //returns address location if character is found
    // if character is not found it returns NULL  
    if(ptr==NULL)
        printf("Character is not present");
    else
        printf("Character is present ");
    return 0;
}*/

/*
int main(void)
{
    char s1[10];
    char s2[10];
    char *ptr=NULL;
    printf("Enter String :");
    scanf("%s",s1);
    printf("Enter String to be searched ");
    scanf("%s",s2);
    ptr=strstr(s1,s2); 
    //returns address location if string is found
    // if string is not found it returns NULL  
    if(ptr==NULL)
        printf("Substring is not present");
    else
        printf("Substring is present ");
    return 0;
}
*/



/*
int main(void)
{
    char s1[10]="abc";
    printf("%s",strrev(s1));
    return 0;
}
*/

/*
int main(void)
{
    char s1[10];
    char s2[10];
    int val;
    printf("Enter First String : ");
    scanf("%s",s1);
    printf("Enter Second String :");
    scanf("%s",s2);
    printf("S1 = %s S2 = %s ",s1,s2);

    val = strcmp(s1,s2);
    printf("Val = %d",val);

    if(val==0)
        printf("Strings are equal");
    else
        printf("Strings are not equal");
    


    return 0;
}
*/

// s1 = abc
//s2 = Abc
// Ascii  a = 97    A = 65   97-65 = 32 (positive difference ) returns 1 

// A = 65  a = 97   65-97= -32  val = -1 


/*
int main(void)
{
    printf("%d",strlen("Akshita Sunbeam")); //15 

}*/


/*
//s1[] = "sunbeam" // sizeof(s1) // 8
//s1[10]="sunbeam" //sizeof(s1)  // 10
//s1[10]="sunbeampune" // sizeof(s1) // 10
// s1[]="sunbeampune" //sizeof(s1) // 12
// s1[]="precat" //sizeof(s1) // 7
// s1[]="precat\0" //sizeof(s1) // 8
//s1[]= "precat" // strlen(s1)  // 6
//s1[]= "precat\0" // strlen(s1)  // 7






// menu driven program 
//0.Exit 1. length 2.copy 3.concat

int main(void)
{
    int choice;
    char s1[20];
    char s2[20];
    int val;
    do
    {   
        printf("\n Enter Choice : 0.Exit 1. length 2.copy 3.concat");
        scanf("%d",&choice);
       
       switch(choice)
       {
           case 1:
                printf("Enter String :");
                scanf("%s",s1);
                val = strlen(s1); //return an int value 
                printf("Length of the string = %d",val);
           break;
           case 2:
                printf("Enter String ");
                scanf("%s",s1);  
                strcpy(s2,s1); // strcpy(destination,source)
                printf("S1 = %s S2=%s",s1,s2);
           break;
           case 3:
                printf("Enter First String ");
                scanf("%s",s1);
                printf("Enter Second String :");
                scanf("%s",s2);

                printf("S1 = %s S2 = %s ",s1,s2);
                strcat(s1,s2); // s1 and s2 and store the result in s1
                printf("S1 = %s",s1);
           break;
       }
    } while (choice!=0);

    
    return 0;
}
*/