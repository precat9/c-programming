








int main()
{
    char ary[]="DiscoveryChannel";
    printf("%s",ary); 
    return 0;
}

A) D
B) DiscoveryChannel
C) Discovery
D) Compiler error


Answer: B


int f(int n); 
main()
{
    int n=10;
    printf("%d",f(n));
}
int f(int n)
{
    if(n>0)
        return(n+f(n-2));
    
}


a) 10
b) 80
c) 30
d) Error

Answer: c
Explanation: The recursive function returns n+f(n-2) till 10>0.
Therefore, the above code will be evaluated as: 10+8+6+4+2, which is equal to 30.



#include <stdio.h>
int main(void)
{
char str[]="SunBeam ITPark";
printf("%s\t%s\t%s",&str[8],&8[str],str+8);
return 0;
}

A) ITPark 	ITPark 	ITPark
B) Compile time error
C) 32 32 IT Park
D) IT Park IT Park SunBeam IT Park


Answer: A


int main(void)
{
char a[100];
a[0]='a';a[1]='b';a[2]='c';a[4]='d';
abc(a);
return 0;
} abc(char a[])
{
a++; printf("%c",*a);
a++; printf("%c",*a);
}
A. ac
B. bc
C. cc
D. cd
Answer: B



#include<stdio.h>
int main( void )
{
void *ptr_name=NULL;
char ch=115, *name="sunbeam";
int j=117;
ptr_name=&ch;
printf("%c", *(char*)ptr_name); 
ptr_name=&j;
printf("%c", *(int*)ptr_name);
ptr_name=name;
printf("%s", (char*)ptr_name+2);
return 0;
}
A. sunbeam
B. s117nbeam
C. 115unbeam
D. 115117unbeam
Answer: A




