
// Display two numeric values on output screen 

#include<stdio.h>

int main(void)
{
    int n1; // variable declaration
    int n2;  // variable declaration 

    n1=45; // variable assignment
    n2=35; // variable assignment 

    printf("n1 value = %d  n2 value = %d",n1,n2);
    printf("\n Num1 : %d Num2 : %d",n1,n2);
    printf("\n N2 = %d N1 = %d ",n1,n2);
    printf("\n First number %d",n1);
    printf(" \n Second Number %d",n2);

    return 0;
}