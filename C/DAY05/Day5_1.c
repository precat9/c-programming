#include<stdio.h>

/*char ch = 50; // 50
signed char ch = 50; // 50
unsigned char ch=50; // 50 

char ch=129; //signed // -127   // 129-256 = -127 
unsigned char ch=129; // unsigned // 129 


char ch=257; // 1          // 257-256 = 1
unsigned char ch=257; // 1 

char ch=254; // -1 
unsigned char ch=254; // 254 

*/










/*
int main(void)
{
    char c3; // signed  -128 to 127 
    char c1=200; // singed -128 to 127
    char c2=50; // signed -128 to 127 
    c3=c1+c2;   // c3 = 200 + 50   c3 = 250   // c3 = -56 +50 //c3 = -6 
    printf("%d",c3); // 250  // 250-256 = -6
    return 0;
}

*/


/*int main(void)
{
    unsigned char c3; // unsigned  0 to 255 
    char c1=200; // singed -128 to 127
    char c2=50; // signed -128 to 127 
    c3=c1+c2;   // c3 = 200 + 50   c3 = 250  
    printf("%d",c3); // 250 
    return 0;
}*/



// unsinged range 0 to 255
// signed range -128 to 127 

/*
int main(void)
{
    //char ch=130;//signed 0 to 127 
    //unsigned char ch=200; // unsigned char range 0 to 255
    //unsigned char ch=456; // unsigned char range 0 to 255
    // 0 to 255 ===> 256
    // 257 ==> 1
    //258 ==>2
    // 456 ==> 200   // 456 - 256 = 200
    char ch=-130; // -130+256 = 126 
    printf("%d",ch);
    return 0;
}

// 260 - 256 
// 4 

*/



/*
int main(void)
{
    char ch=263;
    //256 = 0
    //257 = 1
    //258 = 2
    //259 = 3
    //260 = 4
    // 261 = 5
    // 262 = 6
    //263 = 7

    //shortcut
    // Data - character range 256
    //263 - 256
    // 7  


    // 256 + 256 = 512

    // 514 
    // 0 to 255 
    // 0 to 255 
    //=====
    //512
    //513 == 1
    //514 == 2 


    printf("%d",ch);
    return 0;

}
*/

/*
int main(void)
{
    char ch=130; // by default all char datatypes are considered as signed characters
    // signed -128 to 127

    //128  ==> -128
    //129 ==> -127
    //130 ==> -126   // 130 - 256 = -126
    printf("%d",ch);
   

    return 0;
}

*/