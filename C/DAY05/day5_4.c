#include<stdio.h>
// Relational operators (< , > , <= , >= , == , != (not equal to) )
//compare
// gives the output in the form of 0 (false ) or 1 (true) 

// n1 = 20
// n2 = 5
// n1<n2  = 20<5 = false = 0
// n1>n2  = 20>5 = true = 1
// n1<=n2  less than or equal to 20<=5  false 0
//n1 >= n2 greater than or equal to  20>=5 true 1


// v1 = 5 v2= 5
// v1 == v2  // true // 1
// v1 != v2 // false // 0 

// int a; // variable declaration
// a = 55; // assignment operator 


/*
int main(void)
{
    int n1=20,n2=5;
    int v1=5,v2=5;
    printf("%d < %d = %d",n1,n2,(n1<n2));
    printf("\n n1 <= n2 = %d",(n1<=n2));
    printf("\n n1 > n2 = %d",(n1>n2));
    printf("\n n1 >= n2 = %d",(n1>=n2));
    printf("\n n1 > n2 = %d",(n1>n2));
    printf("\n v1 == v2 = %d",(v1==v2));
    printf("\n v1 != v2 = %d",(v1!=v2));//5!=5 // false 

    return 0;
}
*/