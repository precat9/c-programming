#include<stdio.h>

int main(void)
{
    
    printf("%d %d"); // not getting expected output at execution time / run time
    // Run Time errors
    return 0;
}



/*int main(void)
{
    int a=30;
    int b=20;
    int c = a+b; // LOGIC that we applied is for addition 
    // it is not displayin me result as subtraction 
    printf("A = %d B = %d Subtraction = %d",a,b,c);
    // Logical Error : these errors occurs at the time of writing of wrong logic by the developer 
    return 0;
}
*/


/*
int main(void)
{
    printf("hello"); // linker error
    // used printf() but we have not linked it with its header file 
    // LINKING ERROR 
    // INT_MIN ==> limits.h  
    // INT_MAX 
    return 0;
}
*/


/*
// Compile time error
// wrong syntax

int main(void)
{
    //printf("sunbeam") //semicolon is missing Error : Compile time error 
    //printf("%d",a); // a variable is undeclared : Compile time error
    // we have not mentioned the datatype 

    return 0;
}
*/