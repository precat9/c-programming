#include<stdio.h>


//homework
// number system (binary)
// logic gates OR , AND , NOT 
// Truth table Revise 
// Strictly : Revise basic concepts 


//shorhand operators 
int main(void)
{
    int num=25;
    printf("Num = %d",num); // Num = 25 
    num+=2; //num=num+2; // modified the original num value and store it in the same variable 
    printf("\n Num = %d",num); // Num = 27 
    num-=5;  //num=num-5;
     printf("\n Num = %d",num); // Num = 22
    num*=2; // num = num*2;
    printf("\n Num = %d",num); 
    num/=11; // num=num/11;
    printf("\n Num = %d",num);


    return 0;
}

/*
int main(void)
{
    int num=25;
    printf("Num = %d",num); // Num = 25 
    num=num+2; // modified the original num value and store it in the same variable 
    printf("\n Num = %d",num); // Num = 27 
    num=num-5;
     printf("\n Num = %d",num); // Num = 22
    num = num*2;
    printf("\n Num = %d",num); 
    num=num/11;
    printf("\n Num = %d",num);


    return 0;
}

*/