#include<stdio.h>

//22/7  
// 3.142000 ==> %f
// 3.142 ===>  %.3f 


// int , float ===> float  (output float)
// int , float ==> int  (output int )





/*
int main(void)
{
    int n1,n2;
    float result;
    printf("Enter First number :");
    scanf("%d",&n1);
     printf("Enter Second number :");
    scanf("%d",&n2);
    
    result = n1/n2;
    printf("%f",result);
    return 0;
}
*/

/*
int main(void)
{
    int n1=10,n2=0;
    printf("%d",n1/n2); //10/0 not allowed
    // Run time error 
}*/





// take two numbers as input
// Arithmetic Operator : add, sub,mul,div,mod


// Division : Quotient part = 5/2 = 2
// Mod : remainder part = 5%2 = 1 
// 6/2 = 3
// 6%2 = 0

//7/2 = 3
//7%2 = 1

// 8/3 = 2
//8%3 = 2 

/*
int main(void)
{
    int num1,num2; //declared two variables
    int result;
    printf("Enter First Number :");
    scanf("%d",&num1);
    printf("Enter Second Number :");
    scanf("%d",&num2);
    result = num1+num2;
    printf("\n Addition = %d",result);

    printf("\n Subtraction = %d",num1-num2);
    printf("\n Multiplication = %d",num1*num2);
    printf("\n Division = %d",num1/num2);
    printf("\n Mod = %d",num1%num2);

    return 0;
}

*/