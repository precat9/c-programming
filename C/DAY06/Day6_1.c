#include<stdio.h>


// int num;
// num =45,24,10;
// num ==> 45
// num = 55;
// prinf(num)  ===> 55 



int main(void)
{
    int a=(5,3,17);
    //if we try to allocate more than one value to one variable
    //enclosed inside the round brackets then
    //it assigns right most value to that variable  
    printf("%d",a); //17 
    return 0;
}


/*
int main(void)
{
    int a;
    a=10,20,30; 
    int b;
    b=34,1,19;
    // if we assign multiple values to single variable
    //then left most value is assigned to that variable 
    printf("%d",a); // 10 
      printf("B = %d",b);  // 34

    return 0;
}
*/


/*
int main(void)
{
    int a=10;
    printf("%d",a);
    return 0;
}
*/


/*
int main(void)
{
    int num=20;
    num%=2; // 20 % 2 =0
    int val=35;
    val%=2; // 35 %2 
    printf("%d",num);
     printf("\n val %d",val);
    return 0;
}

*/