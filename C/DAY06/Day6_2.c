#include<stdio.h>

//--

// In case of pre operation (increment / decrement)
// first operation is performed and then 
// the value is assigned to the variable



/*int main(void)
{
    int num=45;
    int val=++num; // Pre Increment
    // pre operation on num 
    // num = 46
    printf("Num = %d Val = %d",num,val);

    int n1=30;
    int res = --n1; // Pre Decrement 
    //pre operation on n1
    // n1 = 29 
    printf("\n N1 = %d Res = %d",n1,res);

    return 0;
}*/


//++

// In case of post operation (increment / decrement)
// first value is assigned to the variable
//then operation is perfomed 


int main(void)
{
    int num=45;
    int val=num++; // Post Increment
    // post operation on num 
    // num = 46
    printf("Num = %d Val = %d",num,val);

    int n1=30;
    int res = n1--; // Post Decrement 
    //post operation on n1
    // n1 = 29 
    printf("\n N1 = %d Res = %d",n1,res);

    return 0;
}

