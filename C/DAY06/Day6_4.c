#include<stdio.h>



int main(void)
{
    int a=12;
    int b=8;
    int c = a ^ b; // bitwise XOR  
    // a = 12   1 1 0 0
    // b = 8    1 0 0 0
    //=====================
    // a|b      0 1 0 0   // 4 
    // if both the bits are same answer is zero
    //if both the bits are different answer is 1  
    printf("C = %d",c);
    return 0;
}


/*
// binary 
// 12 = 1 1 0 0
// 8  = 1 0 0 0 

int main(void)
{
    int a=12;
    int b=8;
    int c = a | b;  // bitwise OR 
    // a = 12   1 1 0 0
    // b = 8    1 0 0 0
    //=====================
    // a|b      1 1 0 0   // 12
    printf("C = %d",c);
    return 0;
}
*/



/*
int main(void)
{
    int a=12;
    int b=8;
    int c = a & b;  // Bit wise AND 
    // a = 12   1 1 0 0
    // b = 8    1 0 0 0
    //=====================
    // a&b      1 0 0 0   // 8 
    printf("C = %d",c);
    return 0;
}
*/