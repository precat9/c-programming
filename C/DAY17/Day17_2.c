#include<stdio.h>


/*
// argc = argument count  (int)
// argv[] = arguement vector (string / char * / character array)
// env[] = environment variables (string /char * / character array)
int main(int argc,char *argv[],char *env[])
{
    int i;
    for(i=0;i<40;i++)
        printf("\n Env[%d] = %s",i,env[i]);
    return 0;
}

*/


/*

// ./a.exe 10 20  
//output : 10+20 = 30

// ./a.exe 30 30 20  
// output : 80 

//argv[i] is in the char * format (String format)
// we can not add directly strings
// so we need to convert string to int 
// for that we have a predefined function 
// atoi()
// used to convert string to int (typecasting)

int main(int argc,char *argv[])
{
    int i;
    int sum=0;
    for(i=1;i<argc;i++)
        sum=sum+atoi(argv[i]); //String to integer 
        // ./a.exe 10 20  
        //argv[0] = a.exe
        //argv[1] = 10
        // argv[2] = 20 
        //sum = 0 
        //i = 1 sum = 0+10 sum=10 
        //i = 2 sum = sum+argv[2] sum = 10 + 20  //sum = 30
     printf("Addition = %d",sum);

    return 0;
}
*/



/*
int main(int argc,char *argv[])
{
    printf("Argument Count = %d",argc);
    int i;
    for(i=0;i<argc;i++)
        printf("\n Argv[%d] = %s",i,argv[i]);

    return 0;
}
*/

