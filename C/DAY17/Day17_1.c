#include<stdio.h>


int main(void)
{
    int arr[3][3]={{11,12,13},{14,15,16},{17,18,19}}; // size = 36 bytes
    int *p[4]= {*arr,*(arr+1),*(arr+2)};
    //printf("%d %d",arr[0][0],**p); //11
    //printf("%d %d",arr[0][1],*(*p+1)); //12 
    //printf("%d %d",arr[0][2],*(*p+2));  //13 

    //("%d ",*(*(p+1)+1)); //15 


    //printf("%d ",*(*(p+2)+2)); //19
    // printf("%d ",(*(*(p+2)+2))+5); // 19 + 5  = 24 

    //printf("%d ",*(*(p+3)+5)); // wrong location // Segmentation Fault //Run time error

    //printf("%d",*(*(p+2))+*(*(p+1))); // 31  // 17 + 14 = 31 
    //printf("%d ",*(*(p+2)+2)+10);  //19 + 10 = 29


    return 0;
}

/*

int main(void)
{
    int arr[3][3]={{11,12,13},{14,15,16},{17,18,19}}; // size = 36 bytes
    int *p[4]= {*arr,*(arr+1),*(arr+2)};
    printf("arr = %u &arr = %d",arr,&arr);
    printf("\n arr+1 = %u &arr+1 = %u",arr+1,&arr+1);
    printf("\n arr+2 = %u &arr+2 = %u",arr+2,&arr+2);
    printf("\n arr+3 = %u &arr+3 = %u",arr+3,&arr+3);
    return 0;
}

*/
 /* arr = 2686748 &arr = 2686748
 arr+1 = 2686760 &arr+1 = 2686784
 arr+2 = 2686772 &arr+2 = 2686820
 arr+3 = 2686784 &arr+3 = 2686856*/