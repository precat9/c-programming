#include<stdio.h>

/*
// ## is used for combining the arguments
#define combine(a,b) a##b
int main(void)
{
    int a=30,b=20,ab=700;
    printf("A = %d B = %d AB = %d ",a,b,combine(a,b)); // ab 
    printf("\n %d",combine(20,10));
    return 0;
}
*/
/*
int main(void)
{
    int a=30,b=20,ab=700;
    printf("A = %d B = %d AB = %d ",a,b,ab);
    return 0;
}


*/


//str Akshita  
//#str  "Akshita" 
//#str+3 "Akshita" + 3

/*
#define STRDISP(x) {printf("\n %s",#x+3);}

int main(void)
{
    STRDISP(sunbeam); //macro call with one argument 
    //printf("\n %s ","sunbeam"+3); 
    return 0;
}*/




// in case of macro
// #represents the string 
// if we would like to display string 
//then we use #symbol 
/*#define STRDISP(x) {printf("\n %s",#x);}

int main(void)
{
    STRDISP(sunbeam); //macro call with one argument 
    //printf("\n %s ","sunbeam"); 
    return 0;
}*/



/*
#define SWAP(type,a,b) {type temp; temp=a; a=b; b=temp;}

int main(void)
{
    int n1=10,n2=5;
    printf("Before : N1 = %d N2 = %d",n1,n2); // 10   5 
    SWAP(int,n1,n2); // MACRO CALL ==> IT will get expanded 
    //SWAP(int,n1,n2);
    //SWAP(type,a,b)
    // type = int a = n1 = 10   b = n2 = 5
    //{type temp; temp=a; a=b; b=temp;}
    //{int temp; temp=n1; n1=n2; n2=temp; }
    // {int temp; temp=10;  n1=5;  n2=10; }

    printf("\n After  : N1 = %d N2 = %d",n1,n2); // 5 10 
    return 0;
}
*/


/*
#define SQR(a) a*a
int main(void)
{
    printf("%d",SQR(3+2));
    //printf("%d",3+2 * 3+2);
    //printf("%d", 3 + 6 +2);
    // 11 
    return 0;
}
*/

/*
#define SQR(a) a*a // Macro with Arguments
// name of macro  is SQR(a) // one argument macro 
// replaceable text  a*a 

int main(void)
{
    printf("%d",SQR(3));
    // printf("%d",3*3);
    //printf("%d",9);
    //9
    // SQR(3) Macro Call
    // Macro call will expand the Macro with its definition

    return 0;
}
*/

/*#define PI 3.14 // Macro definition without arguments
int main(void)
{
    printf("PI = %f",PI);
    PI++; //error: lvalue(locator value) required as increment operand
    return 0;
}*/



/*#define PI 3.14 // preprocessor / Macro Definition
// #define MACRONAME REPLACEABLE_TEXT

// whenever we compile the code
// PI value will get replaced with 3.14 everywhere in the program 


int main(void)
{
    printf("%f",PI);
    // PI will be expanded / replaced with its REPLACEABLE_TEXT
    return 0;
}

*/