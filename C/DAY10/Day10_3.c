#include<stdio.h>
// Objective
// write a function
// sum_numbers() 
// it should take two values as arguments
// and give me addition 

void sum_numbers(int,int); //function declaration
//void sum_numbers(int a,int b); //valid declaration 

int main(void)
{
    int n1=30,n2=20;
    int n3=40,n4=25;
    printf("Inside Main ");
    sum_numbers(n1,n2); //while giving a function call
    // we have passed two values inside round brackets
    // two values are called as Arguments / Parameters 
    //sum_numbers(30,20);
    printf("\n Back to main ");
    sum_numbers(n3,n4);
    printf("\n Main ends");
    //sum_numbers(40,25); //call by value concept 

    return 0;
}

void sum_numbers(int a,int b) // a, b FORMAL ARGUMENTS 
{
    printf("\n Addition = %d",a+b);
    
}



