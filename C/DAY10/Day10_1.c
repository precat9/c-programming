//#include<stdio.h>


/*
//enum{account,security}; // anonymous enum 
//typedef enum{admin,security}account; // anonynomous enum with typedef 
//typedef enum ACCOUNTTYPES{admin,security}account; //enum with typedef 



//can i have only nick name to enum
typedef enum{COMP,IT,CIVIL,MECH,ENTC}DEPT;
// real name : nothing
// nick name : DEPT 

int main(void)
{
    //printf("%d",COMP); // 0 
    DEPT val; // val is a variable of enum type created using nick name 
    val=IT;
    printf("%d",val); //1 
    return 0;
}

*/


/*
enum{RED,GREEN,BLUE}; 
//if we dont give name to enum it is called as anonymous enum
enum{BOOK,LAPTOP,ELECTRONIC};

//RED 0 GREEN 1 BLUE 2
int main(void)
{
    printf("%d",GREEN); //accessed enum element directly
   //if in our program we have anonymous enum
   //then we can not create a variable of type enum
    return 0;
}
*/




/*
typedef enum PROGRAMMINGLANGUAGES{C,CPP,JAVA,PYTHON}LANG;

int main(void)
{
    enum PROGRAMMINGLANGUAGES v1;
    v1=PYTHON;
    printf("\n %d",v1); //3 

    LANG v2;
    v2=C;
    printf("\n %d ",v2);
    return 0;

}
*/


/*
#include<stdio.h>

typedef enum DEPARTMENTS{ADMIN,TRAINING,PLACEMENT}DEPT;

// real name : enum DEPARTMENTS 
// nick name : DEPT 

int main(void)
{
    //printf("%d",TRAINING);//1
    enum DEPARTMENTS d1;//d1 is a variable created using real name
    d1=ADMIN; 
    printf(" %d ",d1); // 0
    d1=TRAINING;
    printf("\n %d ",d1); //1 
    d1=PLACEMENT; 
    printf(" \n %d ",d1); // 2


    DEPT d2; //d2 variable using nick name
     d2=ADMIN; 
    printf("\n %d ",d2); // 0
    d2=TRAINING;
    printf("\n %d ",d2); //1 
    d2=PLACEMENT; 
    printf(" \n %d ",d2); // 2

    return 0;
}
*/




//Test Your C Skills (book) MCQ 
// Let us C book (Concepts + programs)


#include<stdio.h>

int main(void)
{

    typedef int NUMBER;
    int num=50; // one variable of int type real name
    NUMBER val=55; // one variable (val) of int type by nick name
    printf("%d",val);
    printf("\n %d",num);
    return 0;
}


