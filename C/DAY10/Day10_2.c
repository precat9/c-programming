#include<stdio.h>
void add(); //function declaration (global area)
void sub(); 
int main(void)
{
    add(); // function call // main is the caller of the function
    add(); //function call 
    add(); //function call 
    sub(); 
    
    return 0;    
}
// function body 
// function definition
void add()
{
    int n1;
    int n2;
    printf("\n Enter Number :");
    scanf("%d",&n1);
    printf("\n Enter Number : ");
    scanf("%d",&n2);
    printf("\n Addition = %d",n1+n2);
} // function scope 
void sub()
{
    int n1;
    int n2;
    printf("\n Enter Number :");
    scanf("%d",&n1);
    printf("\n Enter Number : ");
    scanf("%d",&n2);
    printf("\n Subtraction = %d",n1-n2);
} 




//if we wish to perform repetative task in the program
// then we use functions


/*
//in this program we are trying to add two numbers
// same code is written three times 
int main(void)
{
    int n1;
    int n2;
    int n3;
    int n4;
    int n5;
    int n6;
    printf("Enter Number ");
    scanf("%d",&n1);
    printf("Enter Number ");
    scanf("%d",&n2);
    printf("\n Addition = %d",n1+n2);
    printf("\n Enter Number ");
    scanf("%d",&n3);
    printf("Enter Number ");
    scanf("%d",&n4);
    printf("\n Addition = %d",n3+n4);
    printf("\n Enter Number ");
    scanf("%d",&n5);
    printf("Enter Number ");
    scanf("%d",&n6);
    printf("\n Addition = %d",n5+n6);
    return 0;
}

*/