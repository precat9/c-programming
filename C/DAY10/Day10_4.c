#include<stdio.h>

//printf("hello"); // hello internally count 5character 
//int val = printf("hello"); //val = 5 
//printf() is having return type as int 

int add(int a,int b); //function declaration
// int add(int,int);

int main(void)
{
    int n1=30,n2=15;
    int result;
    result = add(n1,n2); //function call // n1,n2 (actual values) // Actual arguments
    // result is a variable who is treated as a location
    //for storing the value returned by the function
   printf("Result = %d",result);
    return 0; //0 meaning is program is successfully exited  EXIT SUCCESS
}
//add(n1,n2);
//add(30,15)
//a = 30 b = 15 
int add(int a,int b)
{
    return (a+b); // datatype of return value is int 
    //return (30+15);
    //return 45; // this value is returned to the caller 
}


